import 'package:badges/badges.dart';
import 'package:bigbuy/Custom/Bootom_NavigationBar/bootom_navigation_bar.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/custom_drawer.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/end_drawer.dart';
import 'package:bigbuy/constants.dart';
import 'package:bigbuy/custom_page_view.dart';
import 'package:bigbuy/screens/home/home.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:jiffy/jiffy.dart';

class MyWallet extends StatefulWidget {
  const MyWallet({super.key});

  @override
  State<MyWallet> createState() => _MyWalletState();
}
final _key=GlobalKey<ScaffoldState>();
Box? box = Hive.box('productBox');
class _MyWalletState extends State<MyWallet> {
  String? firstPickedDate;
  String? secondPickedDate;
  double textFontSize = 16.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(

          onPressed: () {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => CustomPageView(selectedIndex: 0),));
          },child: Icon(Icons.home,size: 27,)),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: CustomNavigationBarPage(
          Home_color: Colors.white,
          Produc_tColor: Colors.white,
          Category_color: Colors.white,
          setting_Color: Colors.black),

      key: _key,
      endDrawer: End_Add_to_cart_Drawer(),
      drawer: Custom_Drawer_Page(),
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(onPressed:(){
          _key.currentState!.openDrawer();
        }, icon: Icon(Icons.menu,size: 25,color: Colors.black87,),
        ),
        title: Text("BigBuy",style: GoogleFonts.poppins(
          fontSize: 18,
          fontStyle: FontStyle.italic,
          letterSpacing: 1,
          fontWeight: FontWeight.bold,
          color: Colors.black,
        ),),

        actions: [
          IconButton(
            iconSize: 28,
            icon: Icon(Icons.search,size: 25,color: Colors.black87,),
            onPressed: () {},
          ),
          Badge(
            position: BadgePosition.custom(
              top: 1,
              end: 5,
            ),
            badgeContent: Text("${box!.length}"),
            child: IconButton(
                onPressed: () {
                  _key.currentState!.openEndDrawer();
                },
                icon: Icon(Icons.shopping_cart,size: 25,color: Colors.black87,)),
          ),
        ],
      ),
      backgroundColor: scaffoldColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 30,
                width: double.infinity,
                color: Color(0xff002A56),
                child: Center(
                  child: Text(
                    'My Wallet History',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              SizedBox(
                height: 45,
                child: GestureDetector(
                  onTap: (() {
                    _selectedDate();
                  }),
                  child: TextFormField(
                    enabled: false,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      suffixIcon: Icon(
                        Icons.calendar_month,
                        color: Colors.black87,
                      ),
                      border: OutlineInputBorder(borderSide: BorderSide.none),
                      hintText: firstPickedDate == null
                          ? Jiffy(DateTime.now()).format("dd - MMM - yyyy")
                          : firstPickedDate,
                      hintStyle: TextStyle(fontSize: 14, color: Colors.black87),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return null;
                      }
                      return null;
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Center(
                child: Text(
                  "To",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: textFontSize,
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 45,
                child: GestureDetector(
                  onTap: (() {
                    _selectedDate2();
                  }),
                  child: TextFormField(
                    enabled: false,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      suffixIcon: Icon(
                        Icons.calendar_month,
                        color: Colors.black87,
                      ),
                      border: OutlineInputBorder(borderSide: BorderSide.none),
                      hintText: secondPickedDate == null
                          ? Jiffy(DateTime.now()).format("dd - MMM - yyyy")
                          : secondPickedDate,
                      hintStyle: TextStyle(fontSize: 14, color: Colors.black87),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return null;
                      }
                      return null;
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _selectedDate() async {
    final selectedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime(2050));
    if (selectedDate != null) {
      setState(() {
        firstPickedDate = Jiffy(selectedDate).format("dd - MMM - yyyy");
      });
    }
  }

  _selectedDate2() async {
    final selectedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime(2050));
    if (selectedDate != null) {
      setState(() {
        secondPickedDate = Jiffy(selectedDate).format("dd - MMM - yyyy");
      });
    }
  }
}

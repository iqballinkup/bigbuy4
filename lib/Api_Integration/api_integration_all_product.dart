import 'dart:convert';

import 'package:bigbuy/API_Model_Class/all_barnd_model_class.dart';
import 'package:bigbuy/API_Model_Class/all_product_model_class.dart';
import 'package:bigbuy/API_Model_Class/category_name_list_model.dart';
import 'package:bigbuy/constants.dart';
import 'package:http/http.dart' as http;

  class Api_All_Product_Integration{

    static Future<dynamic> getOrder(context) async{
      List<All_Product_Model> dataList = [];
      try{
        String link="${Baseurl}get_web_products";
        Uri myUri = Uri.parse(link);
        http.Response response = await http.get(myUri,);
     //   print(response.body);
        if(response.statusCode == 200){
          final item = json.decode(response.body);
          All_Product_Model datainstance;
          for(var i in item){
            datainstance = All_Product_Model.fromJson(i);
            dataList.add(datainstance);
            // print("orders: $dataList");
          }
        }
        else{
          print('Data not found');
        }
      } catch(e){
        print(e);
      }
      return dataList;
    }

////////////////////////////////////Brand Api Integration ////////////////////////
   static getBrand(context)async{
      List<Brand_List_Model_Class> brandlist=[];
      try{
        String url="${Baseurl}get_web_brands";
        http.Response response=await http.get(Uri.parse(url));
        // print(response.body);

        if(response.statusCode==200){
          print(response.statusCode);
         var item=jsonDecode(response.body);
          Brand_List_Model_Class brand_list_model_class;
         for(var i in item){
           brand_list_model_class=Brand_List_Model_Class.fromJson(i);
           brandlist.add(brand_list_model_class);
         }
        }
      }catch(error){
        print("Your branding list Api Catch error $error");
      }
     return brandlist;
   }

////////////////////////////////////All Category Name List Make Api Integration/////////////
   static getAllCategoryNameList(context)async{
     List<Category_Name_List_Model> Category_name_list=[];
     String url="${Baseurl}get_web_categories";
      try{
        http.Response response=await http.get(Uri.parse(url),);
        // print(response.body);
        if(response.statusCode==200){
          print(response.statusCode);
          Category_Name_List_Model category_name_list_model;
          var item=jsonDecode(response.body);
          print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
      //    print(item);
          print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
          for(var i in item){
            category_name_list_model=Category_Name_List_Model.fromJson(i);
            Category_name_list.add(category_name_list_model);
          }
        }
      }catch(error){
        print("Your get category Name list  Catch Errror $error");
      }
      return Category_name_list;
   }

}
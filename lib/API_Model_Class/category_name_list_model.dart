class Category_Name_List_Model {
  String? _productCategorySlNo;
  String? _productCategoryName;
  String? _productCategoryImage;
  String? _status;
  String? _addBy;
  String? _addTime;
  String? _updateBy;
  String? _updateTime;
  String? _categoryBranchid;

  Category_Name_List_Model(
      {String? productCategorySlNo,
        String? productCategoryName,
        String? productCategoryImage,
        String? status,
        String? addBy,
        String? addTime,
        String? updateBy,
        String? updateTime,
        String? categoryBranchid}) {
    if (productCategorySlNo != null) {
      this._productCategorySlNo = productCategorySlNo;
    }
    if (productCategoryName != null) {
      this._productCategoryName = productCategoryName;
    }
    if (productCategoryImage != null) {
      this._productCategoryImage = productCategoryImage;
    }
    if (status != null) {
      this._status = status;
    }
    if (addBy != null) {
      this._addBy = addBy;
    }
    if (addTime != null) {
      this._addTime = addTime;
    }
    if (updateBy != null) {
      this._updateBy = updateBy;
    }
    if (updateTime != null) {
      this._updateTime = updateTime;
    }
    if (categoryBranchid != null) {
      this._categoryBranchid = categoryBranchid;
    }
  }

  String? get productCategorySlNo => _productCategorySlNo;
  set productCategorySlNo(String? productCategorySlNo) =>
      _productCategorySlNo = productCategorySlNo;
  String? get productCategoryName => _productCategoryName;
  set productCategoryName(String? productCategoryName) =>
      _productCategoryName = productCategoryName;
  String? get productCategoryImage => _productCategoryImage;
  set productCategoryImage(String? productCategoryImage) =>
      _productCategoryImage = productCategoryImage;
  String? get status => _status;
  set status(String? status) => _status = status;
  String? get addBy => _addBy;
  set addBy(String? addBy) => _addBy = addBy;
  String? get addTime => _addTime;
  set addTime(String? addTime) => _addTime = addTime;
  String? get updateBy => _updateBy;
  set updateBy(String? updateBy) => _updateBy = updateBy;
  String? get updateTime => _updateTime;
  set updateTime(String? updateTime) => _updateTime = updateTime;
  String? get categoryBranchid => _categoryBranchid;
  set categoryBranchid(String? categoryBranchid) =>
      _categoryBranchid = categoryBranchid;

  Category_Name_List_Model.fromJson(Map<String, dynamic> json) {
    _productCategorySlNo = json['ProductCategory_SlNo'];
    _productCategoryName = json['ProductCategory_Name'];
    _productCategoryImage = json['ProductCategory_Image'];
    _status = json['status'];
    _addBy = json['AddBy'];
    _addTime = json['AddTime'];
    _updateBy = json['UpdateBy'];
    _updateTime = json['UpdateTime'];
    _categoryBranchid = json['category_branchid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ProductCategory_SlNo'] = this._productCategorySlNo;
    data['ProductCategory_Name'] = this._productCategoryName;
    data['ProductCategory_Image'] = this._productCategoryImage;
    data['status'] = this._status;
    data['AddBy'] = this._addBy;
    data['AddTime'] = this._addTime;
    data['UpdateBy'] = this._updateBy;
    data['UpdateTime'] = this._updateTime;
    data['category_branchid'] = this._categoryBranchid;
    return data;
  }
}

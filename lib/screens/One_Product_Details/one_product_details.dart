import 'package:badges/badges.dart';
import 'package:bigbuy/Custom/Bootom_NavigationBar/bootom_navigation_bar.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/custom_drawer.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/end_drawer.dart';
import 'package:bigbuy/FooterSection/about_section.dart';
import 'package:bigbuy/HiveDemoWithCart/product.dart';
import 'package:bigbuy/Provider/All_Product_Provider/all_product_provider.dart';
import 'package:bigbuy/custom_page_view.dart';
import 'package:bigbuy/screens/CheckOut/riaz_check_out_page.dart';
import 'package:bigbuy/screens/Login_SignUp/pages/signin_page.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class One_Product_Details extends StatefulWidget {
  One_Product_Details({
    Key? key,
    required this.mainImage,
    required this.mainPrice,
    required this.cashbackAmount,
    required this.productName,
    required this.qty,
    this.subCategoryId,
    required this.productCode,
    this.status,
    this.ipAddress,
    this.addBy,
    this.addTime,
    this.brand,
    this.brandName,
    this.cashbackPercent,
    this.isFeatured,
    this.isHotDeals,
    this.isNewArrival,
    this.productBranchid,
    this.productCategoryID,
    this.productCategoryName,
    required this.productDescription,
    this.productShippingReturns,
    required this.productSlNo,
    this.productSubCategoryName,
    this.productSubSubCategoryName,
    required this.salePrice,
    this.slug,
    this.stock,
    this.subSubCategoryId,
    this.unitID,
    this.unitName,
    this.updateBy,
    this.updateTime,
  }) : super(key: key);
  String? productSlNo;
  String? productCode;
  String? productName;
  String? slug;
  String? productCategoryID;
  String? subCategoryId;
  String? subSubCategoryId;
  String? brand;
  String? salePrice;
  String? mainPrice;
  String? cashbackPercent;
  String? cashbackAmount;
  String? productDescription;
  String? productShippingReturns;
  String? stock;
  String? unitID;
  String? mainImage;
  String? isFeatured;
  String? isHotDeals;
  String? isNewArrival;
  String? status;
  String? addBy;
  String? addTime;
  String? updateBy;
  String? updateTime;
  String? ipAddress;
  String? productBranchid;
  String? qty;
  String? productCategoryName;
  String? productSubCategoryName;
  String? productSubSubCategoryName;
  String? brandName;
  String? unitName;
  @override
  State<One_Product_Details> createState() => _One_Product_DetailsState();
}

class _One_Product_DetailsState extends State<One_Product_Details> {
  int image_select = 0;
  int Size_select = 0;
  int? Size_ID = 1, Collor_ID;
  String? get_image_url;
  final _key = GlobalKey<ScaffoldState>();
  Box? box = Hive.box('productBox');

  @override
  void initState() {
    Provider.of<All_Product_Provider>(context, listen: false)
        .Api_Model_Product_Collor_demo(
         context, int.parse("${widget.productSlNo}"));
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("key ${GetStorage().read("key")}");
    print("Customer_SlNo ${GetStorage().read("Customer_SlNo")}");
    print("Customer_Name ${GetStorage().read("Customer_Name")}");
    print("Customer_Mobile ${GetStorage().read("Customer_Mobile")}");
    print("Customer_Address ${GetStorage().read("Customer_Address")}");
    print("country_id ${GetStorage().read("country_id")}");
    print("image_name ${GetStorage().read("image_name")}");
    print("Customer_Email ${GetStorage().read("Customer_Email")}");
    print("wallet_balance ${GetStorage().read("wallet_balance")}");
    print("referral_id ${GetStorage().read("referral_id")}");
    final All_new_arrivel_product_list =
        Provider.of<All_Product_Provider>(context).all_new_arrivel_product_List;
    final Api_Product_colos = Provider.of<All_Product_Provider>(context)
        .Api_Model_Product_Collor_list;
    final Api_Product_colos_wise_size_List =
        Provider.of<All_Product_Provider>(context)
            .Product_Color_id_wise_size_Model_Class_list;

    return Scaffold(
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => CustomPageView(selectedIndex: 0),
                ));
          },
          child: Icon(
            Icons.home,
            size: 27,
          )),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: CustomNavigationBarPage(
          Home_color: Colors.white,
          Produc_tColor: Colors.black,
          Category_color: Colors.white,
          setting_Color: Colors.white),
      key: _key,
      endDrawer: End_Add_to_cart_Drawer(),
      drawer: Custom_Drawer_Page(),
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back,
            size: 25,
            color: Colors.black87,
          ),
        ),
        title: Text(
          "BigBuy",
          style: GoogleFonts.poppins(
            fontSize: 18,
            fontStyle: FontStyle.italic,
            letterSpacing: 1,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        actions: [
          IconButton(
            iconSize: 28,
            icon: Icon(
              Icons.search,
              size: 25,
              color: Colors.black87,
            ),
            onPressed: () {},
          ),
          Badge(
            position: BadgePosition.custom(
              top: 1,
              end: 5,
            ),
            badgeContent: Text("${box!.length}"),
            child: IconButton(
                onPressed: () {
                  _key.currentState!.openEndDrawer();
                },
                icon: Icon(
                  Icons.shopping_cart,
                  size: 25,
                  color: Colors.black87,
                )),
          ),
        ],
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        padding: EdgeInsets.only(
          left: 15,
          right: 15,
        ),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: [
              Container(
                height: 300,
                width: double.infinity,
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.red,
                          image: DecorationImage(
                              image: AssetImage("assets/icons/me.png"),
                              fit: BoxFit.fill),
                        ),
                        child: Image.network(
                          "https://bigbuy.com.bd/uploads/products/small_image/${widget.mainImage}",
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      width: 100,
                      child: ListView.builder(
                        itemCount: Api_Product_colos.length,
                        itemBuilder: (context, index) {
                          return Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.all(10),
                            //    color: Colors.blue,
                            child: Container(
                              height: 70,
                              width: 70,
                              margin: EdgeInsets.only(
                                left: 2,
                                top: 5,
                              ),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                image: DecorationImage(
                                    image: AssetImage("assets/icons/me.jpg"),
                                    fit: BoxFit.fill),
                              ),
                              child: Image.network(
                                "https://bigbuy.com.bd/uploads/products/colors/small_image/${Api_Product_colos[index].image1}",
                                fit: BoxFit.fill,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 30,
                width: double.infinity,
                alignment: Alignment.centerLeft,
                child: Text(
                  "${widget.productName}",
                  style: GoogleFonts.poppins(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              ////////////////////////Product Code ////////
              Row(
                children: [
                  Text(
                    "Code : ",
                    style: GoogleFonts.poppins(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      letterSpacing: 1,
                    ),
                  ),
                  Text("${widget.productCode}"),
                ],
              ),
              ////////////////////////Product Price ////////
              Row(
                children: [
                  Text("Price : ",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        letterSpacing: 1,
                      )),
                  Text("৳${widget.salePrice}",
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w400,
                        letterSpacing: 0,
                        color: Colors.red,
                      )),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    "৳${widget.mainPrice}",
                    style: TextStyle(
                      decoration: TextDecoration.lineThrough,
                      fontSize: 17,
                      fontWeight: FontWeight.w300,
                      letterSpacing: 0,
                      color: Colors.red,
                    ),
                  ),
                ],
              ),
              ////////////////////////Product Color ////////
              Container(
                child: Row(
                  children: [
                    Text("Color : ",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          letterSpacing: 1,
                        )),
                    Expanded(
                      child: Container(
                        height: 70,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: Api_Product_colos.length,
                          itemBuilder: (context, index) {
                            print(
                                "Api_Product_colos id ${Api_Product_colos[0].colorId}");
                            Collor_ID = int.parse(
                                "${Api_Product_colos[index].colorId}");
                            return InkWell(
                              onTap: () {
                                setState(() {
                                  Provider.of<All_Product_Provider>(context,
                                          listen: false)
                                      .Api_Product_collor_wise_Size(
                                          context,
                                          int.parse("${widget.productSlNo}"),
                                          int.parse(
                                              "${Api_Product_colos[index].colorId}"));
                                  image_select = index;
                                  get_image_url =
                                      "${Api_Product_colos[index].image1}";
                                  Size_select = 0;
                                  Collor_ID = int.parse(
                                      "${Api_Product_colos[index].colorId}");
                                  print(
                                      "Collor idddddddddddddddd====${Collor_ID}");
                                });
                              },
                              child: Container(
                                margin: EdgeInsets.all(10),
                                color: image_select == index
                                    ? Colors.blue
                                    : Colors.white,
                                child: Container(
                                  margin: EdgeInsets.all(5),
                                  height: 65,
                                  width: 65,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: NetworkImage(
                                              "https://bigbuy.com.bd/uploads/products/colors/small_image/${Api_Product_colos[index].image1}"),
                                          fit: BoxFit.fill)),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    )
                  ],
                ),
              ),
              ////////////////////////Product Size ////////
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Row(
                  children: [
                    Text("Size : ",
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          letterSpacing: 1,
                        )),
                    Expanded(
                      child: Container(
                        height: 30,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: Api_Product_colos_wise_size_List.length,
                          itemBuilder: (context, index) {
                            return Container(
                              margin: EdgeInsets.only(left: 10),
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    Size_select = index;
                                    Size_ID = int.parse(
                                        "${Api_Product_colos_wise_size_List[index].sizeId}");
                                    print(
                                        "SIze IDDDDDDDDDDDDDDDDDDDDD ${Size_ID}");
                                  });
                                },
                                child: CircleAvatar(
                                  backgroundColor: Size_select == index
                                      ? Colors.pink
                                      : Colors.blue,
                                  radius: 15,
                                  child: Text(
                                    '${Api_Product_colos_wise_size_List[index].sizeName}',
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              ////////////////////////Product Quantity ////////
              Container(
                margin: EdgeInsets.only(top: 5),
                height: 35,
                width: double.infinity,
                // color: Colors.pink,
                alignment: Alignment.center,
                child: Row(
                  children: [
                    Text(
                      "Quantity :",
                      style: GoogleFonts.poppins(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        letterSpacing: 1,
                      ),
                    ),
                    Row(
                      children: [
                        IconButton(
                            onPressed: () {
                              setState(() {
                                if (countt == 0) {
                                  countt = 0;
                                } else {
                                  countt--;
                                  mainprice =
                                      double.parse("${widget.salePrice}") *
                                          countt;
                                  cashbackAmmount =
                                      double.parse("${widget.mainPrice}") *
                                          countt;
                                  totalmainprice =
                                      (cashbackAmmount! - mainprice!);
                                  print("mainprice : $mainprice");
                                  print("cashbackAmmount : $cashbackAmmount");
                                  print("totalmainprice : $totalmainprice");
                                }
                              });
                            },
                            icon: Icon(
                              Icons.remove_circle_outline_outlined,
                              color: Colors.grey,
                            )),
                        Text("${countt}"),
                        IconButton(
                            onPressed: () {
                              setState(() {
                                countt++;

                                mainprice =
                                    double.parse("${widget.salePrice}") *
                                        countt;
                                cashbackAmmount =
                                    double.parse("${widget.mainPrice}") *
                                        countt;

                                totalmainprice =
                                    (cashbackAmmount! - mainprice!);

                                print("mainprice : $mainprice");
                                print("cashbackAmmount : $cashbackAmmount");
                                print("totalmainprice : $totalmainprice");
                              });
                            },
                            icon: Icon(
                              Icons.add_circle_outline,
                              color: Colors.grey,
                            )),
                      ],
                    ),
                  ],
                ),
              ),
              //////////////////////// Buy section or Add to cart /////////////////
              Container(
                margin: EdgeInsets.only(left: 10, right: 10, top: 10),
                height: 40,
                width: double.infinity,
                alignment: Alignment.center,
                child: Row(
                  children: [
                    ///////////////////////////////////////////Add top cart /////////////
                    Expanded(
                        child: Container(
                      height: 40,
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                          )),
                      alignment: Alignment.center,
                      child: InkWell(
                        onTap: () {
                          bool found = false;
                          setState(() {
                            box!.length;
                          });
                          for (int i = 0; i < box!.length; i++) {
                            ProductDetails existingProduct = box!.getAt(i);
                            if (existingProduct.product_id == widget.productSlNo  ) {
                              //   existingProduct.productQuantity = int.parse("${widget.qty}");
                              existingProduct.productQuantity = countt;
                              box!.putAt(i, existingProduct);
                              found = true;
                              break;
                            }
                          }
                          if (!found) {
                            ProductDetails productDetails = ProductDetails(
                              // produColor: 0xff01568,
                              // productsize: "18.00",
                              //  productQuantity: int.parse("${widget.qty}"),
                              productQuantity: countt,
                              productImage:
                                  "https://bigbuy.com.bd/uploads/products/small_image/${widget.mainImage}",
                              productName: "${widget.productName}",
                              productPrice: double.parse("${widget.salePrice}"),
                              //new dummy values added here
                              cashback_percent: "${widget.cashbackPercent}",
                              color_id: "${Collor_ID}",
                              main_price: double.parse("${widget.mainPrice}"),
                              product_id: "${widget.productSlNo}",
                              size_id: "${Size_ID}",
                            );
                            box!.add(productDetails);
                          }
                        },
                        child: Text(
                          "Add to Cart",
                          style: TextStyle(fontSize: 16, color: Colors.white),
                        ),
                      ),
                    )),
                    SizedBox(
                      width: 10,
                    ),
                    ////////////////////////////////////////Buy Now ////////////////////
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            mainprice =
                                double.parse("${widget.salePrice}") * countt;
                            cashbackAmmount =
                                double.parse("${widget.mainPrice}") * countt;
                            totalmainprice = (cashbackAmmount! - mainprice!);
                            print("mainprice : $mainprice");
                            print("cashbackAmmount : $cashbackAmmount");
                            print("totalmainprice : $totalmainprice");
                          });
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => GetStorage()
                                            .read("key") ==
                                        null
                                    ? SignInPage(
                                        isRemembered: true, emphone: "01")
                                    : CheckoutScreen(
                                        checkoutFrom: "productDetailsPage",
                                        productName: widget.productName,
                                        productQuantity: countt,
                                        productSubtotalPrice: mainprice,
                                        productTotalPrice:
                                            double.parse("${widget.mainPrice}"),
                                        productUnitPrice:
                                            double.parse("${widget.salePrice}"),
                                        Collor_id: Collor_ID,
                                        Size_id: Size_ID,
                                        cashbackammount: cashbackAmmount,
                                        totalmainprice: totalmainprice,
                                        cashBackPersen: widget.cashbackPercent,
                                        Product_id: widget.productSlNo,
                                      ),
                              ));
                        },
                        child: Container(
                          height: 40,
                          decoration: BoxDecoration(
                              color: Colors.pink,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                bottomRight: Radius.circular(20),
                              )),
                          alignment: Alignment.center,
                          child: Text(
                            "Buy Now",
                            style: TextStyle(fontSize: 16, color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              //////////////////////// Description Section ////////////////
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Description : ",
                    style: GoogleFonts.poppins(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      letterSpacing: 1,
                    ),
                  ),
                  Divider(
                    height: 1,
                    indent: 1,
                    thickness: 1,
                    color: Colors.black,
                    endIndent: 1,
                  ),
                  Container(
                      child: Text(
                    Bidi.stripHtmlIfNeeded("${widget.productDescription}"),
                    style: TextStyle(fontSize: 12),
                  )),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              //////////////////////// Related Product Section ////////////////
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Related Product : ",
                    style: GoogleFonts.poppins(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      letterSpacing: 1,
                    ),
                  ),
                  Divider(
                    height: 1,
                    indent: 1,
                    thickness: 1,
                    color: Colors.black,
                    endIndent: 1,
                  ),
                ],
              ),
              //////////////////////// Related Product Section ////////////////
              Container(
                margin: EdgeInsets.only(top: 10),
                width: double.infinity,
                child: GridView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 3,
                    mainAxisSpacing: 16,
                    crossAxisSpacing: 16,
                    mainAxisExtent: 170,
                  ),
                  itemCount: 8,
                  scrollDirection: Axis.vertical,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => One_Product_Details(
                                qty: All_new_arrivel_product_list[index].qty,
                                mainImage: All_new_arrivel_product_list[index]
                                    .mainImage,
                                mainPrice: All_new_arrivel_product_list[index]
                                    .mainPrice,
                                cashbackAmount:
                                    All_new_arrivel_product_list[index]
                                        .cashbackAmount,
                                productName: All_new_arrivel_product_list[index]
                                    .productName,
                                productCode: All_new_arrivel_product_list[index]
                                    .productCode,
                                productDescription:
                                    All_new_arrivel_product_list[index]
                                        .productDescription,
                                salePrice: All_new_arrivel_product_list[index]
                                    .salePrice,
                                productSlNo: All_new_arrivel_product_list[index]
                                    .productSlNo,
                              ),
                            ));
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                child: Stack(
                                  children: [
                                    Image.network(
                                        "https://bigbuy.com.bd/uploads/products/small_image/${All_new_arrivel_product_list[index].mainImage}",
                                        width:
                                            MediaQuery.of(context).size.width,
                                        height: 182),
                                    Positioned(
                                        top: 3,
                                        right: 10,
                                        child: CircleAvatar(
                                          radius: 13,
                                          backgroundColor: Colors.pink,
                                          child: Text(
                                            "${All_new_arrivel_product_list[index].cashbackPercent}",
                                            style: TextStyle(fontSize: 9),
                                          ),
                                        )),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(height: 12),
                            Container(
                              padding: EdgeInsets.only(left: 3, right: 3),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    child: Text(
                                      "${All_new_arrivel_product_list[index].productName}",
                                      style: const TextStyle(
                                        color: Color(0xFF212121),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  Row(
                                    children: [
                                      Text(
                                        'Price :',
                                        style: TextStyle(
                                            fontSize: 11,
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xFF212121)),
                                      ),
                                      Text(
                                        '${All_new_arrivel_product_list[index].salePrice}',
                                        style: TextStyle(
                                            fontSize: 11,
                                            fontWeight: FontWeight.w500,
                                            color: Color(0xFF212121)),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 3),
                                  Container(
                                    height: 22,
                                    width: double.infinity,
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                                // color: Colors.pink,
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                border: Border.all(
                                                    width: 1,
                                                    color: Colors.pink)),
                                            child: Text(
                                              "Add cart",
                                              style: TextStyle(
                                                  color: Colors.pink,
                                                  fontSize: 11),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                          child: Container(
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                border: Border.all(
                                                  color: Color(0xff3399FF),
                                                  width: 1,
                                                )),
                                            alignment: Alignment.center,
                                            child: Text(
                                              "buy now",
                                              style: TextStyle(
                                                  color: Color(0xff3399FF),
                                                  fontSize: 11),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
              //////////////////////About Section/////////////////////////////\
              SizedBox(
                height: 20,
              ),
              BigBuyFooter(),
            ],
          ),
        ),
      ),
    );
  }

  int? isSize = 0;
  int countt = 1;
  double? mainprice, cashbackAmmount, totalmainprice;
}

import 'package:badges/badges.dart';
import 'package:bigbuy/Custom/Bootom_NavigationBar/bootom_navigation_bar.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/custom_drawer.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/end_drawer.dart';
import 'package:bigbuy/constants.dart';
import 'package:bigbuy/custom_page_view.dart';
import 'package:bigbuy/screens/home/home.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive_flutter/adapters.dart';

class Invite extends StatefulWidget {
  const Invite({super.key});

  @override
  State<Invite> createState() => _InviteState();
}

class _InviteState extends State<Invite> {
  TextEditingController inviteLinkController = TextEditingController();
  TextEditingController inviteSmsController = TextEditingController();
  TextEditingController inviteEmailController = TextEditingController();

  double textFormFieldHeight = 40.0;
  double textFontSize = 16.0;
final _key=GlobalKey<ScaffoldState>();
  Box? box = Hive.box('productBox');
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        floatingActionButton: FloatingActionButton(

            onPressed: () {
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => CustomPageView(selectedIndex: 0),));
            },child: Icon(Icons.home,size: 27,)),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: CustomNavigationBarPage(
            Home_color: Colors.white,
            Produc_tColor: Colors.white,
            Category_color: Colors.white,
            setting_Color: Colors.black),

        key: _key,
        endDrawer: End_Add_to_cart_Drawer(),
        drawer: Custom_Drawer_Page(),
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(onPressed:(){
            _key.currentState!.openDrawer();
          }, icon: Icon(Icons.menu,size: 25,color: Colors.black87,),
          ),
          title: Text("BigBuy",style: GoogleFonts.poppins(
            fontSize: 18,
            fontStyle: FontStyle.italic,
            letterSpacing: 1,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),),

          actions: [
            IconButton(
              iconSize: 28,
              icon: Icon(Icons.search,size: 25,color: Colors.black87,),
              onPressed: () {},
            ),
            Badge(
              position: BadgePosition.custom(
                top: 1,
                end: 5,
              ),
              badgeContent: Text("${box!.length}"),
              child: IconButton(
                  onPressed: () {
                    _key.currentState!.openEndDrawer();
                  },
                  icon: Icon(Icons.shopping_cart,size: 25,color: Colors.black87,)),
            ),
          ],
        ),
        backgroundColor: scaffoldColor,
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 30,
                  width: double.infinity,
                  color: Color(0xff002A56),
                  child: Center(
                    child: Text(
                      'Invite',
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  'Invitation Link',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: textFontSize,
                    color: Colors.black54,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: SizedBox(
                        height: textFormFieldHeight,
                        child: TextFormField(
                          controller: inviteLinkController,
                          enabled: false,
                          decoration: InputDecoration(
                            filled: true,
                            contentPadding: EdgeInsets.only(
                                left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                            fillColor: Color.fromARGB(255, 234, 231, 231),
                            border: OutlineInputBorder(),
                            hintText:
                                "https://bigbuy.com.bd/invitation/4965290897",
                            hintStyle: TextStyle(fontSize: 14),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return null;
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 3,
                    ),
                    Container(
                      height: 40,
                      color: Color.fromARGB(255, 218, 215, 215),
                      child: IconButton(
                        icon: const Icon(Icons.copy),
                        onPressed: () {
                          // ...
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  'Invite Via SMS',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: textFontSize,
                    color: Colors.black54,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: SizedBox(
                        height: textFormFieldHeight,
                        child: TextFormField(
                          controller: inviteSmsController,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            filled: true,
                            contentPadding: EdgeInsets.only(
                                left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                            fillColor: Colors.white,
                            border: OutlineInputBorder(),
                            hintText: "Enter Mobile Number",
                            hintStyle: TextStyle(fontSize: 14),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return null;
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 3,
                    ),
                    Container(
                      height: 40,
                      color: Color.fromARGB(255, 218, 215, 215),
                      child: IconButton(
                        icon: const Icon(Icons.send),
                        onPressed: () {
                          // ...
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  'Invite Via EMail',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: textFontSize,
                    color: Colors.black54,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: SizedBox(
                        height: textFormFieldHeight,
                        child: TextFormField(
                          controller: inviteEmailController,
                          decoration: InputDecoration(
                            filled: true,
                            contentPadding: EdgeInsets.only(
                                left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                            fillColor: Colors.white,
                            border: OutlineInputBorder(),
                            hintText: "Enter Email",
                            hintStyle: TextStyle(fontSize: 14),
                          ),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return null;
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 3,
                    ),
                    Container(
                      height: textFormFieldHeight,
                      color: Color.fromARGB(255, 218, 215, 215),
                      child: IconButton(
                        icon: const Icon(Icons.send),
                        onPressed: () {
                          // ...
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Custom_Item_Section extends StatelessWidget {
   Custom_Item_Section({Key? key,required this.Name,required this.onTap,required this.icon, required this.iconn}) : super(key: key);
  String ? Name;
  VoidCallback  ? onTap;
   final Widget  icon;
   Widget iconn;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap:  onTap,
      child: Container(
        margin: EdgeInsets.only(
          left: 5,right: 5,
          top: 5
        ),
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),
          color: Colors.white,
        ),
        padding: EdgeInsets.only(
            left: 10,right: 10
        ),
        height: 40,
        width: MediaQuery.of(context).size.width,
        // color: Colors.purple[100],
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                icon,
                SizedBox(
                  width: 10,
                ),
                Text("${Name}",style: GoogleFonts.poppins(fontSize: 14,fontWeight: FontWeight.w500),),
              ],
            ),
            iconn
          ],
        ),
      ),
    );
  }
}

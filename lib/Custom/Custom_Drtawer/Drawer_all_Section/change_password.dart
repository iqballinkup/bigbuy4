import 'package:badges/badges.dart';
import 'package:bigbuy/Custom/Bootom_NavigationBar/bootom_navigation_bar.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/custom_drawer.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/end_drawer.dart';
import 'package:bigbuy/constants.dart';
import 'package:bigbuy/custom_page_view.dart';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive_flutter/adapters.dart';


class ChangePassword extends StatefulWidget {
  const ChangePassword({super.key});

  @override
  State<ChangePassword> createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  double textFormFieldHeight = 40.0;
  double textFontSize = 16.0;

  bool _obscureText1 = true;
  bool _obscureText2 = true;
  bool _obscureText3 = true;

  TextEditingController oldPassController = TextEditingController();
  TextEditingController newPassController = TextEditingController();
  TextEditingController confirmPassController = TextEditingController();

  void _toggle1() {
    setState(() {
      _obscureText1 = !_obscureText1;
    });
  }

  void _toggle2() {
    setState(() {
      _obscureText2 = !_obscureText2;
    });
  }

  void _toggle3() {
    setState(() {
      _obscureText3 = !_obscureText3;
    });
  }
  Box? box = Hive.box('productBox');
final _key=GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => CustomPageView(selectedIndex: 0),));
          },child: Icon(Icons.home,size: 27,)),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: CustomNavigationBarPage(
          Home_color: Colors.white,
          Produc_tColor: Colors.white,
          Category_color: Colors.white,
          setting_Color: Colors.black),

      key: _key,
      endDrawer: End_Add_to_cart_Drawer(),
      drawer: Custom_Drawer_Page(),
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(onPressed:(){
          _key.currentState!.openDrawer();
        }, icon: Icon(Icons.menu,size: 25,color: Colors.black87,),
        ),
        title: Text("BigBuy",style: GoogleFonts.poppins(
          fontSize: 18,
          fontStyle: FontStyle.italic,
          letterSpacing: 1,
          fontWeight: FontWeight.bold,
          color: Colors.black,
        ),),

        actions: [
          IconButton(
            iconSize: 28,
            icon: Icon(Icons.search,size: 25,color: Colors.black87,),
            onPressed: () {},
          ),
          Badge(
            position: BadgePosition.custom(
              top: 1,
              end: 5,
            ),
            badgeContent: Text("${box!.length}"),
            child: IconButton(
                onPressed: () {
                  _key.currentState!.openEndDrawer();
                },
                icon: Icon(Icons.shopping_cart,size: 25,color: Colors.black87,)),
          ),
        ],
      ),
      backgroundColor: scaffoldColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 30,
                width: double.infinity,
                color: Color(0xff002A56),
                child: Center(
                  child: Text(
                    'Change Password',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "Old Password",
                style: TextStyle(
                    color: Colors.black54,
                    fontSize: textFontSize,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: textFormFieldHeight,
                child: Align(
                  alignment: Alignment.center,
                  child: TextFormField(
                    controller: oldPassController,
                    obscureText: _obscureText1,
                    decoration: InputDecoration(
                        filled: true,
                        contentPadding: EdgeInsets.only(
                            left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        suffixIcon: IconButton(
                          icon: Icon(_obscureText1
                              ? Icons.visibility_off
                              : Icons.visibility),
                          onPressed: () {
                            _toggle1();
                          },
                        )),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return null;
                      }
                      return null;
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "New Password",
                style: TextStyle(
                    color: Colors.black54,
                    fontSize: textFontSize,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: textFormFieldHeight,
                child: TextFormField(
                  controller: newPassController,
                  obscureText: _obscureText2,
                  decoration: InputDecoration(
                      filled: true,
                      contentPadding: EdgeInsets.only(
                          left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                      fillColor: Colors.white,
                      border: OutlineInputBorder(),
                      suffixIcon: IconButton(
                        icon: Icon(_obscureText2
                            ? Icons.visibility_off
                            : Icons.visibility),
                        onPressed: () {
                          _toggle2();
                        },
                      )),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return null;
                    }
                    return null;
                  },
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "Confirm Password",
                style: TextStyle(
                    color: Colors.black54,
                    fontSize: textFontSize,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: textFormFieldHeight,
                child: Align(
                  alignment: Alignment.center,
                  child: TextFormField(
                    controller: confirmPassController,
                    obscureText: _obscureText3,
                    decoration: InputDecoration(
                        filled: true,
                        contentPadding: EdgeInsets.only(
                            left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        suffixIcon: IconButton(
                          icon: Icon(_obscureText3
                              ? Icons.visibility_off
                              : Icons.visibility),
                          onPressed: () {
                            _toggle3();
                          },
                        )),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return null;
                      }
                      return null;
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Align(
                  alignment: Alignment.centerRight,
                  child:
                      ElevatedButton(onPressed: () {}, child: Text("Update")))
            ],
          ),
        ),
      ),
    );
  }
}

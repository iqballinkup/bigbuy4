
import 'package:bigbuy/HiveDemoWithCart/product.dart';
import 'package:bigbuy/Provider/All_Product_Provider/all_product_provider.dart';
import 'package:bigbuy/Provider/SubCategory_Provider/all_sub_category_product.dart';
import 'package:bigbuy/custom_page_view.dart';


import 'package:bigbuy/screens/home/home.dart';
import 'package:flutter/material.dart';

import 'package:hive/hive.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:provider/provider.dart';

//
// void main() {
//   runApp(const FreshBuyerApp());
// }

void main() async {
  // Initialize hive
  await Hive.initFlutter();
  // Registering the adapter
  WidgetsFlutterBinding.ensureInitialized();
  Hive.registerAdapter(ProductDetailsAdapter());
  // Opening the box
  await Hive.openBox('productBox');
  runApp(const FreshBuyerApp());
}

class FreshBuyerApp extends StatelessWidget {
  const FreshBuyerApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<All_Product_Provider>(
            create: (context) => All_Product_Provider()),
        ChangeNotifierProvider<All_SubCategory_Product_Provider>(
            create: (context) => All_SubCategory_Product_Provider()),
      ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Fresh-Buyer',
      //   home: HomeScreen(title: "Home")
          home: CustomPageView(
            selectedIndex: 0,
          ),

          ),
    );
  }
}

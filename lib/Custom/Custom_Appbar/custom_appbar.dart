import 'package:badges/badges.dart';
import 'package:bigbuy/Provider/All_Product_Provider/all_product_provider.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';





class Custom_Appbar extends StatelessWidget {
    Custom_Appbar({Key? key,required this.onPressed,required this.End_Add_To_Cart_Drawer}) : super(key: key);
    Box? box = Hive.box('productBox');
  final VoidCallback? onPressed,End_Add_To_Cart_Drawer;

  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: EdgeInsets.only(top: 0),
      sliver: SliverAppBar(
        backgroundColor: Colors.white,
        leading: IconButton(onPressed: onPressed, icon: Icon(Icons.menu,size: 25,color: Colors.black87,),
        ),
        title: Text("BigBuy",style: GoogleFonts.poppins(
          fontSize: 18,
          fontStyle: FontStyle.italic,
          letterSpacing: 1,
          fontWeight: FontWeight.bold,
          color: Colors.black,
        ),),
        pinned: true,
        actions: [
          IconButton(
          iconSize: 28,
          icon: Icon(Icons.search,size: 25,color: Colors.black87,),
          onPressed: () {},
        ),

           Badge(
             position: BadgePosition.custom(
               top: 1,
               end: 5,
             ),
            badgeContent: Consumer<All_Product_Provider>(
                builder: (context, value, child) {
                  return Text("${value.addproductcount}");
                },

            ),
            child:  IconButton(
              iconSize: 28,
              icon:Icon(Icons.shopping_cart,size: 30,color: Colors.black87,),
              onPressed:  End_Add_To_Cart_Drawer,
            ),
          ),



        ],
      ),
    );
  }
}

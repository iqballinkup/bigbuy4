import 'package:bigbuy/Custom/Custom_Drtawer/Drawer_all_Section/my_profile.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/end_drawer.dart';
import 'package:bigbuy/Provider/All_Product_Provider/all_product_provider.dart';
import 'package:bigbuy/screens/All_Category/all_category.dart';
import 'package:bigbuy/screens/All_Product/all_product.dart';
import 'package:bigbuy/screens/home/home.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:provider/provider.dart';

import 'screens/Login_SignUp/pages/signin_page.dart';

class CustomPageView extends StatefulWidget {
   CustomPageView({super.key,required this.selectedIndex});
  int selectedIndex;
  @override
  _CustomPageViewState createState() => _CustomPageViewState();
}

class _CustomPageViewState extends State<CustomPageView> {
  final PageController _controller = PageController();

   @override
  void initState() {
     Provider.of<All_Product_Provider>(context,listen: false).Api_all_Companyin_formation(context);
     Future.delayed(Duration(milliseconds: 100),(){
       if (_controller.hasClients) {
         _controller.jumpToPage(widget.selectedIndex);
       }
     });

     // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(

        resizeToAvoidBottomInset: false,
        backgroundColor: Color(0xffD4EAFF),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              widget.selectedIndex = 0;
              if (_controller.hasClients) {
                _controller.jumpToPage(widget.selectedIndex);
              }
            });
            print("Home button is tapped");
          },
          backgroundColor: Color.fromARGB(255, 22, 123, 246),
          child: Icon(
            Icons.home,
            color: widget.selectedIndex == 0 ? Colors.black : Colors.white,
            size: 27,),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        bottomNavigationBar: Container(
          height: 50,
          color: Color(0xff076BD6),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              InkWell(
                onTap: () {
                  setState(() {
                    widget.selectedIndex = 1;
                  });
                  if (_controller.hasClients) {
                    _controller.jumpToPage(widget.selectedIndex);
                  }
                  print("Product button is tapped");
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.production_quantity_limits,
                      color:
                      widget.selectedIndex == 1 ? Colors.black : Colors.white,
                      size: 23,
                    ),
                    Text(
                      "Product",
                      style: TextStyle(
                        color: widget.selectedIndex == 1
                            ? Colors.black
                            : Colors.white,
                      ),
                    )
                  ],
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    widget.selectedIndex = 2;
                      _controller.jumpToPage(widget.selectedIndex);
                  });
                  print("Category button is tapped");
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.category,
                      color:
                      widget.selectedIndex == 2 ? Colors.black : Colors.white,
                      size: 23,
                    ),
                    Text(
                      "Category",
                      style: TextStyle(
                        color: widget.selectedIndex == 2
                            ? Colors.black
                            : Colors.white,
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                width: 30.0,
              ),

              GestureDetector(
                onTap: () {
                  setState(() {
                    widget.selectedIndex = 3;
                    if (_controller.hasClients) {
                      _controller.jumpToPage(widget.selectedIndex);
                    }
                  });
                  print("Account button is tapped");
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.person,
                      color:
                      widget.selectedIndex == 3 ? Colors.black : Colors.white,
                      size: 23,
                    ),
                    Text(
                      "Account",
                      style: TextStyle(
                        color: widget.selectedIndex == 3
                            ? Colors.black
                            : Colors.white,
                      ),
                    )
                  ],
                ),
              ),

              GestureDetector(
                onTap: () {
                  setState(() {
                    widget.selectedIndex = 4;
                    if (_controller.hasClients) {
                      _controller.jumpToPage(widget.selectedIndex);
                    }
                  });
                  print("Cart button is tapped");
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.shopping_cart,
                      color:
                      widget.selectedIndex == 4 ? Colors.black : Colors.white,
                      size: 23,
                    ),
                    Text(
                      "Cart",
                      style: TextStyle(
                        color: widget.selectedIndex == 4
                            ? Colors.black
                            : Colors.white,
                      ),
                    )
                  ],
                ),
              ),

            ],
          ),
        ),

        body: PageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: _controller,
          children: [
            HomeScreen(title: "Home"),
            All_Product_Page(),
            All_CateGory_Page(),
            GetStorage().read("key")==null?SignInPage(isRemembered: false,emphone: "01"):MyProfile(),
            End_Add_to_cart_Drawer(),


          ],
        ),
      ),
    );
  }
}

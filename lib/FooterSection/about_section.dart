
import 'package:bigbuy/Provider/All_Product_Provider/all_product_provider.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class BigBuyFooter extends StatefulWidget {
   BigBuyFooter({super.key});
  @override
  State<BigBuyFooter> createState() => _BigBuyFooterState();
}

class _BigBuyFooterState extends State<BigBuyFooter> {
  List itemColors = [Colors.green, Colors.purple, Colors.blue];

  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width ;
    var screenWidthh = MediaQuery.of(context).size.width/30;
    final imgList=Provider.of<All_Product_Provider>(context).brandList;
    return Container(
      padding:const EdgeInsets.all(0.0),
      color: Colors.black,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          Center(
            child: Image.asset(
              "images/bigbuy.png",
              width: screenWidth / 3,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 100,
            width: double.infinity,
            color: Colors.white,
            child: CarouselSlider.builder(
                itemCount: imgList.length,
                itemBuilder: (context, index, realIndex) {
                  return Container(
                    height: 100,
                    width: 300,
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 0.5,
                        color: Colors.black
                      ),
                    ),
                  child: Image.network("https://bigbuy.com.bd/uploads/brands/${imgList[index].image}",fit: BoxFit.fill,),
                  );
                },
                  options: CarouselOptions(
                  viewportFraction: 0.4,
                  initialPage: 0,
                  enableInfiniteScroll: true,
                  reverse: false,
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 1),
                  autoPlayAnimationDuration: Duration(milliseconds: 700),
                  autoPlayCurve: Curves.fastOutSlowIn,
                  enlargeCenterPage: true,
                  enlargeFactor: 0.5,
                  scrollDirection: Axis.horizontal,
                ),
            )
          ),

          Container(
            margin: EdgeInsets.only(
              left: 20
            ),
            child: Text(
              'Social Media',
              style: GoogleFonts.poppins(
                textStyle: TextStyle(
                  fontSize: screenWidth / 24,
                  color:Colors.white,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.asset(
                "images/fb.png",
                width: screenWidth / 11,
              ),
              SizedBox(
                width: 5.0,
              ),
              Image.asset(
                "images/twitter.png",
                width: screenWidth / 11,
              ),
              SizedBox(
                width: 5.0,
              ),
              Image.asset(
                "images/instagram.png",
                width: screenWidth / 11,
              ),
              SizedBox(
                width: 5.0,
              ),
              Image.asset(
                "images/youtube.png",
                width: screenWidth / 11,
              ),
              SizedBox(
                width: 5.0,
              ),
              Image.asset(
                "images/pinterest.png",
                width: screenWidth / 11,
              ),
            ],
          ),
          SizedBox(
            height: 5.0,
          ),
          Container(
            margin: EdgeInsets.only(
              left: 20
            ),
            alignment: Alignment.center,
            child: Text(
              'Got Question? Call us 24/7',
              style: GoogleFonts.poppins(
                textStyle: TextStyle(
                  fontSize: screenWidth / 25,
                  color: tcolor,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          Container(
            margin: EdgeInsets.only(
              left: 20,
            ),
            alignment: Alignment.center,
            child: Text(
              '01944996633',
              style: GoogleFonts.poppins(
                textStyle: TextStyle(
                  fontSize: screenWidth / 25,
                  color: Colors.blue,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            margin: EdgeInsets.only(
              left: 20,
            ),
            child: Text(
              'Payment Method',
              style: GoogleFonts.poppins(
                textStyle: TextStyle(
                  fontSize: screenWidth / 25,
                  color:tcolor,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 5.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.asset(
                "images/visa.png",
                width: screenWidth / 11,
              ),
              SizedBox(
                width: 5.0,
              ),
              Image.asset(
                "images/mastercard.jpg",
                width: screenWidth / 11,
              ),
              SizedBox(
                width: 5.0,
              ),
              Image.asset(
                "images/paypal.png",
                width: screenWidth / 11,
              ),
              SizedBox(
                width: 5.0,
              ),
              Image.asset(
                "images/american_express.png",
                width: screenWidth / 11,
              ),
              SizedBox(
                width: 5.0,
              ),
              Image.asset(
                "images/apple_pay.png",
                width: screenWidth / 11,
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                  Container(
                    margin: EdgeInsets.only(
                      left: 20,
                      right: 20,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Information',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontSize: screenWidth / 25,
                              fontWeight: FontWeight.w500,
                              color: tcolor,
                            ),
                          ),
                        ),

                        Container(
                          color: ccolor,
                          height: height,
                          child: Text(
                            'About Big Buy',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: screenWidthh ,
                                color: tcolor,
                              ),
                            ),
                          ),
                        ),

                        Container(
                          color: ccolor,
                          height: height,
                          child: Text(
                            'FAQ',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: screenWidthh ,
                                color: tcolor,
                              ),
                            ),
                          ),
                        ),

                        Container(
                          color: ccolor,
                          height: height,
                          child: Text(
                            'Contact us',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: screenWidthh ,
                                color: tcolor,
                              ),
                            ),
                          ),
                        ),

                        Container(
                          color: ccolor,
                          height: height,
                          child: Text(
                            'Log in',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: screenWidthh ,
                                color: tcolor,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  //------------------ My Account ----------------
                  Container(
                    margin: EdgeInsets.only(left: 20,right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'My Account',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontSize: screenWidth / 25,
                              fontWeight: FontWeight.w500,
                              color: tcolor,
                            ),
                          ),
                        ),
                        Container(
                          color: ccolor,
                          height: height,
                          child:  Text(
                            'Sign In',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: screenWidthh ,
                                color: tcolor,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          color: ccolor,
                          height: height,
                          child:  Text(
                            'View Cart',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: screenWidthh ,
                                color: tcolor,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          color: ccolor,
                          height: height,
                          child:  Text(
                            'My Wishlist',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: screenWidthh ,
                                color:tcolor,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          color: ccolor,
                          height: height,
                          child:  Text(
                            'Track My Order',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: screenWidthh ,
                                color: tcolor,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          color: ccolor,
                          height: height,
                          child:  Text(
                            'Help',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: screenWidthh ,
                                color:  tcolor,
                              ),
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
                  //----------------Footer Section--------------
                ],),

                Container(
                  margin: EdgeInsets.only(
                    left: 20,
                    right: 20,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Customer Service',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 25,
                            fontWeight: FontWeight.w500,
                            color: tcolor,
                          ),
                        ),
                      ),
                      Container(
                        color: ccolor,
                        height: height,
                        child: Text(
                          'Payment Methods',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontSize: screenWidthh ,
                              color:  tcolor,
                            ),
                          ),
                        ),
                      ),

                      Container(
                        color: ccolor,
                        height: height,
                        child: Text(
                          'Money-back gurantee!',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontSize: screenWidthh ,
                              color:  tcolor,
                            ),
                          ),
                        ),
                      ),

                      Container(
                        color: ccolor,
                        height: height,
                        child: Text(
                          'Returns',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontSize: screenWidthh ,
                              color:  tcolor,
                            ),
                          ),
                        ),
                      ),

                      Container(
                        color: ccolor,
                        height: height,
                        child: Text(
                          'Shipping',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontSize: screenWidthh ,
                              color:  tcolor,
                            ),
                          ),
                        ),
                      ),

                      Container(
                        color: ccolor,
                        height: height,
                        child: Text(
                          'Terms and conditions',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontSize: screenWidthh ,
                              color:  tcolor,
                            ),
                          ),
                        ),
                      ),

                      Container(
                        color: ccolor,
                        height: height,
                        child: Text(
                          'Privacy Policy',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontSize: screenWidthh ,
                              color:  tcolor,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            color: Colors.black,
            child: Column(
              children: [
                Divider(
                  color: Colors.white,
                ),
                SizedBox(
                  height: 5.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Design & Developed By ',
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          fontSize: screenWidth / 40,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        _launchURL();
                      },
                      child: Text(
                        'Link-Up Technology Ltd',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            decoration: TextDecoration.underline,
                            fontSize: screenWidth / 40,
                            color: Colors.blue,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height:3,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Terms Of Use | ',
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          fontSize: screenWidth / 40,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Text(
                      'Privacy Policy',
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          decoration: TextDecoration.underline,
                          fontSize: screenWidth / 40,
                          color: Colors.blue,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 3,
                ),
                InkWell(
                  onTap: () {

                  },
                  child: Text(
                    'Copyright © 2023 Big Buy. All Rights Reserved.',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontSize: 10,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          )
        ],
      ),
    );

  }

  double height=20;

  Color  tcolor=Colors.white;

  Color  ccolor=Colors.black;
  _launchURL() async {
    const url = 'http://linktechbd.com/';
    final uri = Uri.parse(url);
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri);
    } else {
      throw 'Could not launch $url';
    }
  }
}




import 'package:badges/badges.dart';
import 'package:bigbuy/Custom/Bootom_NavigationBar/bootom_navigation_bar.dart';
import 'package:bigbuy/Custom/Custom_Appbar/custom_appbar.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/custom_drawer.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/end_drawer.dart';
import 'package:bigbuy/FooterSection/about_section.dart';
import 'package:bigbuy/Provider/All_Product_Provider/all_product_provider.dart';
import 'package:bigbuy/Provider/SubCategory_Provider/all_sub_category_product.dart';
import 'package:bigbuy/model/category.dart';
import 'package:bigbuy/screens/All_Category/SubCategoryProduct/sub_category_product.dart';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';


class All_CateGory_Page extends StatefulWidget {
  const All_CateGory_Page({Key? key}) : super(key: key);

  @override
  State<All_CateGory_Page> createState() => _All_CateGory_PageState();
}

class _All_CateGory_PageState extends State<All_CateGory_Page> {
  final _key=GlobalKey<ScaffoldState>();

  //////////////////////////////////////////////fatch_Category_Product_List///////////////////////////////////


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    final all_category_name_list=Provider.of<All_Product_Provider>(context).CategoryNameList;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,

        drawer: Custom_Drawer_Page(),
        endDrawer: End_Add_to_cart_Drawer(),
        key: _key,
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () {
              _key.currentState!.openDrawer();
            },
            icon: Icon(
              Icons.menu,
              size: 25,
              color: Colors.black87,
            ),
          ),
          title: Text(
            "BigBuy",
            style: GoogleFonts.poppins(
              fontSize: 18,
              fontStyle: FontStyle.italic,
              letterSpacing: 1,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
          actions: [
            IconButton(
              iconSize: 28,
              icon: Icon(
                Icons.search,
                size: 25,
                color: Colors.black87,
              ),

              onPressed: () {},
            ),
            Badge(
              position: BadgePosition.custom(
                top: 1,
                end: 5,
              ),
              badgeContent: Text("${box!.length}"),
              child: IconButton(
                  onPressed: () {
                    _key.currentState!.openEndDrawer();
                  },
                  icon: Icon(
                    Icons.shopping_cart,
                    size: 25,
                    color: Colors.black87,
                  )),
            ),
          ],
          elevation: 0.0,
        ),
      //  bottomNavigationBar:CustomNavigationBarPage(Home_color: Colors.grey, Produc_tColor: Colors.grey, Category_color: Colors.white, setting_Color: Colors.grey),
        body: CustomScrollView(
          slivers: [
            ////////////////Custom Apps Bar ///////
            // Custom_Appbar(
            //   onPressed: () {
            //     _key.currentState!.openDrawer();
            //   },
            //   End_Add_To_Cart_Drawer: () {
            //     _key.currentState!.openEndDrawer();
            //   },
            // ),
            ///////////////All Category ///////
            SliverPadding(
                padding: EdgeInsets.only(
                  right: 20,
                  left: 20,
                  bottom:0 ,
                  top: 10,
                ),
              sliver: SliverGrid(
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 185,
                mainAxisExtent: 200,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                ),
                delegate: SliverChildBuilderDelegate((context, index){
                  return InkWell(
                    onTap: () {
                      Provider.of<All_SubCategory_Product_Provider>(context,listen: false).getBrand(context,int.parse("${all_category_name_list[index].productCategorySlNo}"));
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SubCategoryProduct(
                        Categoryname: all_category_name_list[index].productCategoryName,
                        category_Idi:int.parse("${all_category_name_list[index].productCategorySlNo}"),

                      ),));
                      },
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          // color: Colors.amber,
                        ),
                        child: Column(
                          children: [
                            Expanded(
                              flex:1,
                                child: Container(
                                  height: 100,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15),
                                      topRight: Radius.circular(15),
                                    ),

                                        image: DecorationImage(image: AssetImage("images/profile.jpg"),fit: BoxFit.fill),

                                  ),
                                  child:Image.network("https://bigbuy.com.bd/uploads/categories/${all_category_name_list[index].productCategoryImage}" ?? "",fit: BoxFit.fill),
                              ),
                            ),
                            Container(
                              height: 30,
                              color: Color(0xff076BD6),
                              alignment: Alignment.center,
                              child: Text('${all_category_name_list[index].productCategoryName}',style: GoogleFonts.poppins(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                              ),),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
                    childCount: all_category_name_list.length,
                ),
              ),
            ),

            SliverToBoxAdapter(
              child: BigBuyFooter(),
            )
        ],
       ),
      ),
    );
  }

  late final List<Category> categories = homeCategries;
}

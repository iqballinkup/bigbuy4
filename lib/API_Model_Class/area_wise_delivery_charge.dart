class Api_area_wise_delivery_charse {
  String? _districtSlNo;
  String? _deliveryOptionId;
  String? _districtName;
  String? _status;
  String? _addBy;
  String? _addTime;
  Null? _updateBy;
  Null? _updateTime;
  String? _areaBranchid;
  String? _deliveryOptionName;
  String? _deliveryCharge;

  Api_area_wise_delivery_charse(
      {String? districtSlNo,
        String? deliveryOptionId,
        String? districtName,
        String? status,
        String? addBy,
        String? addTime,
        Null? updateBy,
        Null? updateTime,
        String? areaBranchid,
        String? deliveryOptionName,
        String? deliveryCharge}) {
    if (districtSlNo != null) {
      this._districtSlNo = districtSlNo;
    }
    if (deliveryOptionId != null) {
      this._deliveryOptionId = deliveryOptionId;
    }
    if (districtName != null) {
      this._districtName = districtName;
    }
    if (status != null) {
      this._status = status;
    }
    if (addBy != null) {
      this._addBy = addBy;
    }
    if (addTime != null) {
      this._addTime = addTime;
    }
    if (updateBy != null) {
      this._updateBy = updateBy;
    }
    if (updateTime != null) {
      this._updateTime = updateTime;
    }
    if (areaBranchid != null) {
      this._areaBranchid = areaBranchid;
    }
    if (deliveryOptionName != null) {
      this._deliveryOptionName = deliveryOptionName;
    }
    if (deliveryCharge != null) {
      this._deliveryCharge = deliveryCharge;
    }
  }

  String? get districtSlNo => _districtSlNo;
  set districtSlNo(String? districtSlNo) => _districtSlNo = districtSlNo;
  String? get deliveryOptionId => _deliveryOptionId;
  set deliveryOptionId(String? deliveryOptionId) =>
      _deliveryOptionId = deliveryOptionId;
  String? get districtName => _districtName;
  set districtName(String? districtName) => _districtName = districtName;
  String? get status => _status;
  set status(String? status) => _status = status;
  String? get addBy => _addBy;
  set addBy(String? addBy) => _addBy = addBy;
  String? get addTime => _addTime;
  set addTime(String? addTime) => _addTime = addTime;
  Null? get updateBy => _updateBy;
  set updateBy(Null? updateBy) => _updateBy = updateBy;
  Null? get updateTime => _updateTime;
  set updateTime(Null? updateTime) => _updateTime = updateTime;
  String? get areaBranchid => _areaBranchid;
  set areaBranchid(String? areaBranchid) => _areaBranchid = areaBranchid;
  String? get deliveryOptionName => _deliveryOptionName;
  set deliveryOptionName(String? deliveryOptionName) =>
      _deliveryOptionName = deliveryOptionName;
  String? get deliveryCharge => _deliveryCharge;
  set deliveryCharge(String? deliveryCharge) =>
      _deliveryCharge = deliveryCharge;

  Api_area_wise_delivery_charse.fromJson(Map<String, dynamic> json) {
    _districtSlNo = json['District_SlNo'];
    _deliveryOptionId = json['Delivery_option_id'];
    _districtName = json['District_Name'];
    _status = json['status'];
    _addBy = json['AddBy'];
    _addTime = json['AddTime'];
    _updateBy = json['UpdateBy'];
    _updateTime = json['UpdateTime'];
    _areaBranchid = json['area_branchid'];
    _deliveryOptionName = json['delivery_option_name'];
    _deliveryCharge = json['delivery_charge'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['District_SlNo'] = this._districtSlNo;
    data['Delivery_option_id'] = this._deliveryOptionId;
    data['District_Name'] = this._districtName;
    data['status'] = this._status;
    data['AddBy'] = this._addBy;
    data['AddTime'] = this._addTime;
    data['UpdateBy'] = this._updateBy;
    data['UpdateTime'] = this._updateTime;
    data['area_branchid'] = this._areaBranchid;
    data['delivery_option_name'] = this._deliveryOptionName;
    data['delivery_charge'] = this._deliveryCharge;
    return data;
  }
}

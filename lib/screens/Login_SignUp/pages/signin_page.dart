import 'dart:convert';

import 'package:bigbuy/Custom/Bootom_NavigationBar/bootom_navigation_bar.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/custom_drawer.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/end_drawer.dart';
import 'package:bigbuy/constants.dart';
import 'package:bigbuy/custom_page_view.dart';
import 'package:bigbuy/screens/Login_SignUp/pages/before_registration_page.dart';
import 'package:bigbuy/screens/Login_SignUp/pages/otp_generate.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';



class SignInPage extends StatefulWidget {
   SignInPage({super.key,this.emphone,required this.isRemembered});
  String ? emphone;
  bool isRemembered=true;
  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  int _selectedRadioButton = 0;
  bool _obscureText = true;
  bool isRemembered = false;
  var emailPhoneText = "Phone";
  var emailPhoneTextt ="Bangladesh";
  double textFormFieldHeight = 45.0;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  TextEditingController emailPhoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
final _key=GlobalKey<ScaffoldState>();

Fatch_Login()async{
  String link="${Baseurl}customer_login_procedure";
  try{
    Response response=await Dio().post(link,
        data:{
          "customer":{
            "bound":"${emailPhoneTextt}",
            "Customer_Mobile":emailPhoneTextt=="Bangladesh"?"${emailPhoneController.text}" : "",
            "Customer_Email":emailPhoneTextt=="International" ? "${emailPhoneController.text}" :"",
            "password":"${passwordController.text}"
          }
        }
    );
    print(response.data);
   var item= jsonDecode(response.data);
    print("mmmmmmmmmmmmmmmmmmmmmmm ${item["phone_not_verified"]}");
   if(item["success"]==true){
     GetStorage().write("key",  item["id"]["key"]);
     GetStorage().write("Customer_SlNo", item["id"]["customer"]["Customer_SlNo"]);
     GetStorage().write("Customer_Name", item["id"]["customer"]["Customer_Name"]);
     GetStorage().write("Customer_Mobile", item["id"]["customer"]["Customer_Mobile"]);
     GetStorage().write("Customer_Address", item["id"]["customer"]["Customer_Address"]);
     GetStorage().write("country_id", item["id"]["customer"]["country_id"]);
     GetStorage().write("image_name", item["id"]["customer"]["image_name"]);
     GetStorage().write("Customer_Email", item["id"]["customer"]["Customer_Email"]);
     GetStorage().write("wallet_balance", item["id"]["customer"]["wallet_balance"]);
     GetStorage().write("wallet_hang_balance", item["id"]["customer"]["wallet_hang_balance"]);
     GetStorage().write("status", item["id"]["customer"]["status"]);
     GetStorage().write("bound", item["id"]["customer"]["bound"]);
     GetStorage().write("register_bonus", item["id"]["customer"]["register_bonus"]);
     GetStorage().write("referral_id", item["id"]["customer"]["referral_id"]);
     print("sAddsafsf ${GetStorage().read("key")}");
     print("sAddsafsf ${GetStorage().read("Customer_SlNo")}");
     print("sAddsafsf ${GetStorage().read("Customer_Name")}");
     print("sAddsafsf ${GetStorage().read("Customer_Mobile")}");
     print("sAddsafsf ${GetStorage().read("Customer_Address")}");
     print("sAddsafsf ${GetStorage().read("country_id")}");
     print("sAddsafsf ${GetStorage().read("image_name")}");
     print("sAddsafsf ${GetStorage().read("Customer_Email")}");
     print("sAddsafsf ${GetStorage().read("wallet_balance")}");
     print("sAddsafsf ${GetStorage().read("referral_id")}");
     Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => CustomPageView(selectedIndex: 0,)));
     ScaffoldMessenger.of(context).showSnackBar(
         SnackBar(
           duration: Duration(seconds: 1),
             content: Text("Login successfull")

         ));
   }else if(item["phone_not_verified"]==true){
      GetStorage().write("customer_id", item["customer_id"]);
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PinputExample(
        number: emailPhoneController.text,
      ),));
      await Dio().get("https://bigbuy.com.bd/resend-otp/${item["phone_not_verified"]}");
    }

   else{
     ScaffoldMessenger.of(context).showSnackBar(
         SnackBar(
             duration: Duration(seconds: 1),
             content: Text("Invalid username or password")

         ));
   }

  }catch(e){
    print(e);
  }
}

  Fatch_Login_get_customer()async{
    String link="${Baseurl}get_auth_customer";
    try{
      Response response=await Dio().get(link);
      print(response.data);
      var item= jsonDecode(response.data);
      print(item["success"]);

    }catch(e){
      print(e);
    }
  }



@override
  void initState() {
  emailPhoneController.text="${widget.emphone}" ;
  // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      key: _key,

      backgroundColor: Color(0xffD4EAFF),
      body: Center(
        child: Container(
          alignment: Alignment.center,
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(image:AssetImage("images/walpaperr.jpg"),fit: BoxFit.fill)
          ),
          padding: const EdgeInsets.only(left: 20.0,right: 20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Text(
                    'Sign In',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: screenWidth / 15,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Radio(
                      value: 0,
                      groupValue: _selectedRadioButton,
                      onChanged: (value) {
                        setState(() {
                          _selectedRadioButton = value!;
                          emailPhoneText = "Phone";
                          emailPhoneTextt="Bangladesh";
                        });
                      },
                    ),
                    Text(
                      'Bangladesh',
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: screenWidth / 24,
                          color: Colors.black54,
                        ),
                      ),
                    ),
                    Radio(
                      value: 1,
                      groupValue: _selectedRadioButton,
                      onChanged: (value) {
                        setState(() {
                          _selectedRadioButton = value!;
                          emailPhoneText = "Email";
                          emailPhoneTextt="International";
                        });
                      },
                    ),
                    Text(
                      'International',
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: screenWidth / 24,
                          color: Colors.black54,
                        ),
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  emailPhoneText,
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: textFormFieldHeight,
                  child: TextFormField(
                    controller: emailPhoneController,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Invalid";
                      }

                    },
                    decoration: InputDecoration(
                        errorStyle: TextStyle(
                          height: 0.01,
                        ),
                        contentPadding: EdgeInsets.only(
                            left: 10,right: 0,bottom: 0,top: 0
                        ),
                        filled: true,
                        fillColor: Colors.white,
                        border: OutlineInputBorder()),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Password',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: textFormFieldHeight,
                  child: TextFormField(
                    controller: passwordController,
                    obscureText: _obscureText,
                    decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        errorStyle: TextStyle(
                          height: 0.01,
                        ),
                        contentPadding: EdgeInsets.only(
                          left: 10,right: 0,bottom: 0,top: 0
                        ),
                        border: OutlineInputBorder(),
                        suffixIcon: IconButton(
                          icon: Icon(_obscureText
                              ? Icons.visibility_off
                              : Icons.visibility),
                          onPressed: () {
                            _toggle();
                          },
                        )),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Invalid";
                      }

                    },
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  'Forgot Your Password?',
                  style: GoogleFonts.poppins(
                    textStyle: TextStyle(
                      fontSize: screenWidth / 24,
                      color: Colors.black ,
                    ),
                  ),
                ),
                Row(
                  children: [
                    Checkbox(
                      checkColor: Colors.white,
                      activeColor: Colors.black,
                      value: isRemembered,
                      onChanged: (bool? value) {
                        setState(() {
                          isRemembered = value!;
                        });
                      },
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Text(
                      'Remember Me',
                      style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                          fontSize: screenWidth / 24,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () {
                    Fatch_Login();
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'LOG IN',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: screenWidth / 22,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Icon(
                        Icons.forward,
                        color: Colors.white,
                      )
                    ],
                  ),
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red,
                      minimumSize: const Size.fromHeight(40)),
                ),
                SizedBox(
                  height: 5,
                ),
                ////////Sign in sec/////////////
                Center(
                  child: Text(
                    'or sign in with',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontSize: screenWidth / 22,
                        color: Colors.black ,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () {},
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        height: 28,
                        width: 28,
                        "images/google.png",
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        'Login With Google',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 25,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                    ],
                  ),
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.white,
                      minimumSize: const Size.fromHeight(40)),
                ),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  onPressed: () {
                    Fatch_Login_get_customer();
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "images/facebook.png",
                        height: 28,
                        width: 28,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        'Login With Facebook',
                        style: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            fontSize: screenWidth / 25,
                            color: Colors.black54,
                          ),
                        ),
                      ),
                    ],
                  ),
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.white,
                      minimumSize: const Size.fromHeight(40)),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Don\'t have an account?'),
                    SizedBox(
                      width: 5,
                    ),
                    GestureDetector(
                      onTap: (() {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage(),));
                      }),
                      child: Text(
                        'Register now',
                        style: TextStyle(
                          decoration: TextDecoration.underline,
                            color: Colors.red,
                            fontSize: screenWidth / 24,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

}

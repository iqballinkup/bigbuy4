class All_Product_Model {
  String? _productSlNo;
  String? _productCode;
  String? _productName;
  String? _slug;
  String? _productCategoryID;
  String? _subCategoryId;
  String? _subSubCategoryId;
  String? _brand;
  String? _salePrice;
  String? _mainPrice;
  String? _cashbackPercent;
  String? _cashbackAmount;
  String? _productDescription;
  String? _productShippingReturns;
  String? _stock;
  String? _unitID;
  String? _mainImage;
  String? _isFeatured;
  String? _isHotDeals;
  String? _isNewArrival;
  String? _status;
  String? _addBy;
  String? _addTime;
  String? _updateBy;
  String? _updateTime;
  String? _ipAddress;
  String? _productBranchid;
  String? _qty;
  String? _productCategoryName;
  String? _productSubCategoryName;
  String? _productSubSubCategoryName;
  String? _brandName;
  String? _unitName;

  All_Product_Model(
      {
        String? productSlNo,
        String? productCode,
        String? productName,
        String? slug,
        String? productCategoryID,
        String? subCategoryId,
        String? subSubCategoryId,
        String? brand,
        String? salePrice,
        String? mainPrice,
        String? cashbackPercent,
        String? cashbackAmount,
        String? productDescription,
        String? productShippingReturns,
        String? stock,
        String? unitID,
        String? mainImage,
        String? isFeatured,
        String? isHotDeals,
        String? isNewArrival,
        String? status,
        String? addBy,
        String? addTime,
        String? updateBy,
        String? updateTime,
        String? ipAddress,
        String? productBranchid,
        String? qty,
        String? productCategoryName,
        String? productSubCategoryName,
        String? productSubSubCategoryName,
        String? brandName,
        String? unitName,

      }) {
    if (productSlNo != null) {
      this._productSlNo = productSlNo;
    }
    if (productCode != null) {
      this._productCode = productCode;
    }
    if (productName != null) {
      this._productName = productName;
    }
    if (slug != null) {
      this._slug = slug;
    }
    if (productCategoryID != null) {
      this._productCategoryID = productCategoryID;
    }
    if (subCategoryId != null) {
      this._subCategoryId = subCategoryId;
    }
    if (subSubCategoryId != null) {
      this._subSubCategoryId = subSubCategoryId;
    }
    if (brand != null) {
      this._brand = brand;
    }
    if (salePrice != null) {
      this._salePrice = salePrice;
    }
    if (mainPrice != null) {
      this._mainPrice = mainPrice;
    }
    if (cashbackPercent != null) {
      this._cashbackPercent = cashbackPercent;
    }
    if (cashbackAmount != null) {
      this._cashbackAmount = cashbackAmount;
    }
    if (productDescription != null) {
      this._productDescription = productDescription;
    }
    if (productShippingReturns != null) {
      this._productShippingReturns = productShippingReturns;
    }
    if (stock != null) {
      this._stock = stock;
    }
    if (unitID != null) {
      this._unitID = unitID;
    }
    if (mainImage != null) {
      this._mainImage = mainImage;
    }
    if (isFeatured != null) {
      this._isFeatured = isFeatured;
    }
    if (isHotDeals != null) {
      this._isHotDeals = isHotDeals;
    }
    if (isNewArrival != null) {
      this._isNewArrival = isNewArrival;
    }
    if (status != null) {
      this._status = status;
    }
    if (addBy != null) {
      this._addBy = addBy;
    }
    if (addTime != null) {
      this._addTime = addTime;
    }
    if (updateBy != null) {
      this._updateBy = updateBy;
    }
    if (updateTime != null) {
      this._updateTime = updateTime;
    }
    if (ipAddress != null) {
      this._ipAddress = ipAddress;
    }
    if (productBranchid != null) {
      this._productBranchid = productBranchid;
    }
    if (qty != null) {
      this._qty = qty;
    }
    if (productCategoryName != null) {
      this._productCategoryName = productCategoryName;
    }
    if (productSubCategoryName != null) {
      this._productSubCategoryName = productSubCategoryName;
    }
    if (productSubSubCategoryName != null) {
      this._productSubSubCategoryName = productSubSubCategoryName;
    }
    if (brandName != null) {
      this._brandName = brandName;
    }
    if (unitName != null) {
      this._unitName = unitName;
    }
  }

  String? get productSlNo => _productSlNo;
  set productSlNo(String? productSlNo) => _productSlNo = productSlNo;
  String? get productCode => _productCode;
  set productCode(String? productCode) => _productCode = productCode;
  String? get productName => _productName;
  set productName(String? productName) => _productName = productName;
  String? get slug => _slug;
  set slug(String? slug) => _slug = slug;
  String? get productCategoryID => _productCategoryID;
  set productCategoryID(String? productCategoryID) =>
      _productCategoryID = productCategoryID;
  String? get subCategoryId => _subCategoryId;
  set subCategoryId(String? subCategoryId) => _subCategoryId = subCategoryId;
  String? get subSubCategoryId => _subSubCategoryId;
  set subSubCategoryId(String? subSubCategoryId) =>
      _subSubCategoryId = subSubCategoryId;
  String? get brand => _brand;
  set brand(String? brand) => _brand = brand;
  String? get salePrice => _salePrice;
  set salePrice(String? salePrice) => _salePrice = salePrice;
  String? get mainPrice => _mainPrice;
  set mainPrice(String? mainPrice) => _mainPrice = mainPrice;
  String? get cashbackPercent => _cashbackPercent;
  set cashbackPercent(String? cashbackPercent) =>
      _cashbackPercent = cashbackPercent;
  String? get cashbackAmount => _cashbackAmount;
  set cashbackAmount(String? cashbackAmount) =>
      _cashbackAmount = cashbackAmount;
  String? get productDescription => _productDescription;
  set productDescription(String? productDescription) =>
      _productDescription = productDescription;
  String? get productShippingReturns => _productShippingReturns;
  set productShippingReturns(String? productShippingReturns) =>
      _productShippingReturns = productShippingReturns;
  String? get stock => _stock;
  set stock(String? stock) => _stock = stock;
  String? get unitID => _unitID;
  set unitID(String? unitID) => _unitID = unitID;
  String? get mainImage => _mainImage;
  set mainImage(String? mainImage) => _mainImage = mainImage;
  String? get isFeatured => _isFeatured;
  set isFeatured(String? isFeatured) => _isFeatured = isFeatured;
  String? get isHotDeals => _isHotDeals;
  set isHotDeals(String? isHotDeals) => _isHotDeals = isHotDeals;
  String? get isNewArrival => _isNewArrival;
  set isNewArrival(String? isNewArrival) => _isNewArrival = isNewArrival;
  String? get status => _status;
  set status(String? status) => _status = status;
  String? get addBy => _addBy;
  set addBy(String? addBy) => _addBy = addBy;
  String? get addTime => _addTime;
  set addTime(String? addTime) => _addTime = addTime;
  String? get updateBy => _updateBy;
  set updateBy(String? updateBy) => _updateBy = updateBy;
  String? get updateTime => _updateTime;
  set updateTime(String? updateTime) => _updateTime = updateTime;
  String? get ipAddress => _ipAddress;
  set ipAddress(String? ipAddress) => _ipAddress = ipAddress;
  String? get productBranchid => _productBranchid;
  set productBranchid(String? productBranchid) =>
      _productBranchid = productBranchid;
  String? get qty => _qty;
  set qty(String? qty) => _qty = qty;
  String? get productCategoryName => _productCategoryName;
  set productCategoryName(String? productCategoryName) =>
      _productCategoryName = productCategoryName;
  String? get productSubCategoryName => _productSubCategoryName;
  set productSubCategoryName(String? productSubCategoryName) =>
      _productSubCategoryName = productSubCategoryName;
  String? get productSubSubCategoryName => _productSubSubCategoryName;
  set productSubSubCategoryName(String? productSubSubCategoryName) =>
      _productSubSubCategoryName = productSubSubCategoryName;
  String? get brandName => _brandName;
  set brandName(String? brandName) => _brandName = brandName;
  String? get unitName => _unitName;
  set unitName(String? unitName) => _unitName = unitName;

  All_Product_Model.fromJson(Map<String, dynamic> json) {
    _productSlNo = json['Product_SlNo'];
    _productCode = json['Product_Code'];
    _productName = json['Product_Name'];
    _slug = json['slug'];
    _productCategoryID = json['ProductCategory_ID'];
    _subCategoryId = json['sub_category_id'];
    _subSubCategoryId = json['sub_sub_category_id'];
    _brand = json['brand'];
    _salePrice = json['sale_price'];
    _mainPrice = json['main_price'];
    _cashbackPercent = json['cashback_percent'];
    _cashbackAmount = json['cashback_amount'];
    _productDescription = json['Product_description'];
    _productShippingReturns = json['Product_shipping_returns'];
    _stock = json['stock'];
    _unitID = json['Unit_ID'];
    _mainImage = json['main_image'];
    _isFeatured = json['is_featured'];
    _isHotDeals = json['is_hot_deals'];
    _isNewArrival = json['is_new_arrival'];
    _status = json['status'];
    _addBy = json['AddBy'];
    _addTime = json['AddTime'];
    _updateBy = json['UpdateBy'];
    _updateTime = json['UpdateTime'];
    _ipAddress = json['ip_address'];
    _productBranchid = json['Product_branchid'];
    _qty = json['qty'];
    _productCategoryName = json['ProductCategory_Name'];
    _productSubCategoryName = json['ProductSubCategory_Name'];
    _productSubSubCategoryName = json['ProductSubSubCategory_Name'];
    _brandName = json['brand_name'];
    _unitName = json['Unit_Name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Product_SlNo'] = this._productSlNo;
    data['Product_Code'] = this._productCode;
    data['Product_Name'] = this._productName;
    data['slug'] = this._slug;
    data['ProductCategory_ID'] = this._productCategoryID;
    data['sub_category_id'] = this._subCategoryId;
    data['sub_sub_category_id'] = this._subSubCategoryId;
    data['brand'] = this._brand;
    data['sale_price'] = this._salePrice;
    data['main_price'] = this._mainPrice;
    data['cashback_percent'] = this._cashbackPercent;
    data['cashback_amount'] = this._cashbackAmount;
    data['Product_description'] = this._productDescription;
    data['Product_shipping_returns'] = this._productShippingReturns;
    data['stock'] = this._stock;
    data['Unit_ID'] = this._unitID;
    data['main_image'] = this._mainImage;
    data['is_featured'] = this._isFeatured;
    data['is_hot_deals'] = this._isHotDeals;
    data['is_new_arrival'] = this._isNewArrival;
    data['status'] = this._status;
    data['AddBy'] = this._addBy;
    data['AddTime'] = this._addTime;
    data['UpdateBy'] = this._updateBy;
    data['UpdateTime'] = this._updateTime;
    data['ip_address'] = this._ipAddress;
    data['Product_branchid'] = this._productBranchid;
    data['qty'] = this._qty;
    data['ProductCategory_Name'] = this._productCategoryName;
    data['ProductSubCategory_Name'] = this._productSubCategoryName;
    data['ProductSubSubCategory_Name'] = this._productSubSubCategoryName;
    data['brand_name'] = this._brandName;
    data['Unit_Name'] = this._unitName;
    return data;
  }
}

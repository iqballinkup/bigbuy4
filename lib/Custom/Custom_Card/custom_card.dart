import 'package:bigbuy/HiveDemoWithCart/product.dart';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

class Custom_Card_Page extends StatefulWidget {
  Custom_Card_Page({
    Key? key,
    required this.mainImage,
    required this.mainPrice,
  required  this.cashbackAmount,
    required this.productName,
    required this.qty,
    this.subCategoryId,
    required this.productCode,
    this.status,
    this.ipAddress,
    this.addBy,
    this.addTime,
    this.brand,
    this.brandName,
    required this.cashbackPercent,
    this.isFeatured,
    this.isHotDeals,
    this.isNewArrival,
    this.productBranchid,
    this.productCategoryID,
    this.productCategoryName,
    required this.productDescription,
    this.productShippingReturns,
  required  this.productSlNo,
    this.productSubCategoryName,
    this.productSubSubCategoryName,
    required this.salePrice,
    this.slug,
    this.stock,
    this.subSubCategoryId,
    this.unitID,
    this.unitName,
    this.updateBy,
    this.updateTime,
  }) : super(key: key);
  String? productSlNo;
  String? productCode;
  String? productName;
  String? slug;
  String? productCategoryID;
  String? subCategoryId;
  String? subSubCategoryId;
  String? brand;
  String? salePrice;
  String? mainPrice;
  String? cashbackPercent;
  String? cashbackAmount;
  String? productDescription;
  String? productShippingReturns;
  String? stock;
  String? unitID;
  String? mainImage;
  String? isFeatured;
  String? isHotDeals;
  String? isNewArrival;
  String? status;
  String? addBy;
  String? addTime;
  String? updateBy;
  String? updateTime;
  String? ipAddress;
  String? productBranchid;
  String? qty;
  String? productCategoryName;
  String? productSubCategoryName;
  String? productSubSubCategoryName;
  String? brandName;
  String? unitName;

  @override
  State<Custom_Card_Page> createState() => _Custom_Card_PageState();
}

class _Custom_Card_PageState extends State<Custom_Card_Page> {
  late final Box? box;
  @override
  void initState() {
    super.initState();
    // Get reference to an already opened box
    box = Hive.box('productBox');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(20)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Color(0xFFeeeeee),
            ),
            child: Stack(
              children: [
                Container(
                  height: 185,
                  width: 200,
                  decoration: BoxDecoration(image: DecorationImage(image: AssetImage("images/american_express.png"), fit: BoxFit.fill)),
                  child: Image.network(
                    "${widget.mainImage}",
                    fit: BoxFit.fill,
                  ),
                ),
                Positioned(
                    top: 10,
                    right: 16,
                    child: CircleAvatar(
                      radius: 17,
                      backgroundColor: Colors.pink,
                      child: Text(
                        "${widget.cashbackPercent}",
                        style: TextStyle(fontSize: 13),
                      ),
                    )),
              ],
            ),
          ),
          SizedBox(height: 9),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10 ),
              )
            ),
            padding: EdgeInsets.only(left: 3, right: 3),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Text(
                    // "Men  Sleeve Shirt",
                    "${widget.productName}",
                    maxLines: 1,
                    style: const TextStyle(
                      color: Color(0xFF212121),
                      fontWeight: FontWeight.w500,
                      fontSize: 12,

                    ),
                  ),
                ),
                SizedBox(height: 5),
                Row(
                  children: [
                    Text(
                      'Price :',
                      style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Color(0xFF212121)),
                    ),
                    Text(
                      "${double.parse(widget.salePrice!) + 1}",
                      style: TextStyle(fontSize: 13, fontWeight: FontWeight.w500, color: Color(0xFF212121)),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      ' \$${widget.mainPrice}',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w300,
                        color: Color(0xFF212121),
                        decoration: TextDecoration.lineThrough,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 5),
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10 ),
                      )
                  ),
                  height: 30,
                  width: double.infinity,
                  child: Row(
                    children: [
                      Expanded(
                        child: InkWell(
                          onTap: () {
                            bool found = false;
                            setState(() {
                              box!.length;
                            });
                            print("lllllllllllllllllllllllllllll${box!.length}");
                            for (int i = 0; i < box!.length; i++) {
                              ProductDetails existingProduct = box!.getAt(i);
                              print("fffffffffffffffffffff${existingProduct.productName} fffffffffffffffffffff${widget.productName}");
                              print(
                                  "fffffffffffffffffffff${existingProduct.productPrice!.toStringAsFixed(2)} fffffffffffffffffffff${widget.salePrice}");
                              if (existingProduct.product_id == widget.productSlNo){
                                print("CCCCCCCCCCCCCCCCCCCCC${box!.length}");
                                existingProduct.productQuantity = int.parse("${widget.qty}");
                                box!.putAt(i, existingProduct);
                                found = true;
                                break;
                              }
                            }
                            if (!found) {
                              ProductDetails productDetails = ProductDetails(
                                // produColor: 0xff01568,
                                // productsize:" 18.00",
                                productQuantity: int.parse("${widget.qty}"),
                                productImage: "${widget.mainImage}",
                                productName: "${widget.productName}",
                                productPrice: double.parse("${widget.salePrice}"),
                                //new dummy values added here
                                cashback_percent: "${widget.cashbackPercent}",
                                color_id: "1",
                                main_price: double.parse("${widget.mainPrice}"),
                                product_id: "${widget.productSlNo}",
                                size_id: "1",

                              );
                              box!.add(productDetails);
                            }
                          },
                          child: Container(
                            height: 30,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                // color: Colors.pink,
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(width: 1, color: Colors.pink)),
                            child: Text(
                              "Add Cart",
                              style: TextStyle(color: Colors.pink),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Container(
                          height: 30,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: Color(0xff3399FF),
                                width: 1,
                              )),
                          alignment: Alignment.center,
                          child: Text(
                            "Buy Now",
                            style: TextStyle(color: Color(0xff3399FF)),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

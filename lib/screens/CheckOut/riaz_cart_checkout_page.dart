import 'package:bigbuy/Custom/Custom_Drtawer/Drawer_all_Section/my_order.dart';
import 'package:bigbuy/CustomTextFormField/riaz_custom_text_form_field.dart';
import 'package:bigbuy/HiveDemoWithCart/cartdata.dart';
import 'package:bigbuy/Provider/All_Product_Provider/all_product_provider.dart';
import 'package:bigbuy/constants.dart';
import 'package:bigbuy/custom_page_view.dart';
import 'package:bigbuy/screens/Login_SignUp/pages/signin_page.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';

class CartCheckoutPage extends StatefulWidget {
  const CartCheckoutPage({Key? key}) : super(key: key);
  @override
  State<CartCheckoutPage> createState() => _CartCheckoutPageState();
}

class _CartCheckoutPageState extends State<CartCheckoutPage> {
  String? _selectedLocation;
  bool showWalletPay = false;
  String deliveryOptions = "Cash on delivery";
  final _formKey = GlobalKey<FormState>();
  TextEditingController _shipperNameController = TextEditingController();
  TextEditingController _shipperPhoneController = TextEditingController();
  TextEditingController _shipperAddressController = TextEditingController();
  TextEditingController _orderNoteController = TextEditingController();

  final _listViewController = ScrollController();
  final _scrollViewController = ScrollController();
  late final Box box;
  double shippingFee = 60;
  bool isWalletAdjusted = false;
  int _selectedRadioButton = 0;
  double amount=0;
  int vat=0;
  bool ordersuccess=false;

  double cashback = 0;
  int Delivery_Charge=0;
  int wallet_adjustment=0;
  double vat_with_total=0;

  double Cashback = 0;
  double deliveryCharge = 0;
  double vat_deleverychagre_total = 0;
  double wallet_vat_deleverychagre_total = 0;
  double wallet_balance  = 0;


  Fatch_Orderrrrrr(context)async{
    int vat= int.parse("${GetStorage().read("customer_vat")}") as int;
    double totalPrice = 0;
    double totalmainprice = 0;
    double vat_with_total=0;
    for (int i = 0; i < box.length; i++) {
      var productData = box.getAt(i)!;
      totalPrice += productData.productPrice * productData.productQuantity;
      totalmainprice += productData.main_price * productData.productQuantity;
      Cashback=totalmainprice-totalPrice;
      vat_with_total=(vat*totalmainprice)/100;
      vat_deleverychagre_total = vat_with_total+totalPrice+deliveryCharge;
      print("productData.main_price  :${productData.main_price}");
      print("productData.productPrice  :${productData.productPrice}");
      print("tttttttttttttttttttttttttttttttttttttttttttttttttttt $totalmainprice");
      print("ttttttttttttttttttttttttttttttgggggggggggggggggggg $totalPrice");
    }

    String link ="https://bigbuy.com.bd/api/place_order";
    print("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh $vat");
    int Delivery_Charge= int.parse("${GetStorage().read("AAAAAdeliveryCharge")}") as int;
    int wallet_adjustment= int.parse("${GetStorage().read("wallet_adjustment")}") as int;

    vat_with_total=(vat*totalPrice)/100;
    double vat_with_total_with_deliverycharge_=vat_with_total+totalPrice + Delivery_Charge - amount;
    List<cartdata> user=[];
    var jsonUser;
    try{
      for (int i = 0; i < box.length; i++) {
        var productData = box.getAt(i)!;
        user.add(cartdata(
          quantity:  "${productData.productQuantity}",
          cashback:  "${productData.main_price - productData.productPrice}"  ,
          cashback_percent: productData.cashback_percent  ,
          color_id:  productData.color_id  ,
          main_price: "${productData.main_price}" ,
          product_id:  productData.product_id  ,
          purchase_rate:  productData.size_id ,
          size_id:  productData.size_id  ,
          total_cashback: "${productData.productQuantity * (productData.main_price - productData.productPrice) }",
          total_main_price:  "${productData.main_price}" ,
        ),
        );

        print("aaaaaaaaaaaaaaaaaaaaaaaaaaa product price ${user[i].cashback}");
        print("total_cashback  ${user[i].total_cashback}");

        print("total_cashback  ${user[i].quantity}");
        jsonUser = user;
      };


      Response response=await Dio().post(link,
        data:{
          "customer_id" : "${GetStorage().read("Customer_SlNo")}",
          "customer_name" : "${_shipperNameController.text}",
          "wallet_upto_adjustment_percent" :"${GetStorage().read("wallet_adjustment")}",
          "previous_wallet_balance" : "${GetStorage().read("wallet_balance")}",
          "bound" :"${GetStorage().read("bound")}",
          "customer_phone":"${_shipperPhoneController.text}",
          "customer_email" : "${_shipperPhoneController.text}",
          "shipping_address" : "${GetStorage().read("Customer_Address")}",
          "order_note" : "${_orderNoteController.text}",
          "sub_total" :"$vat_deleverychagre_total",
          "cashback" :"$cashback",
          "vat" :vat,
          "delivery_charge" :"${GetStorage().read("AAAAAdeliveryCharge")}",
          "total" :vat_deleverychagre_total-amount,
          "wallet_adjustment_amount" :amount,
          "payable_amount" :vat_deleverychagre_total-amount,
          "payment_type" : _selectedRadioButton==0?"Cash On Delivery" : "Online Payment",
          "is_wallet_adjustment" :showWalletPay==true ?1:0 ,
          "wallet_adjustment_percent":"${GetStorage().read("wallet_adjustment")}",
          "vat_percent" :"${GetStorage().read("customer_vat")}",
          "area_id" :"${GetStorage().read("AAAAAarea_id")}",
          "delivery_option_id":"${GetStorage().read("AAAAAdeliveryOptionId")}",
          "cart" :user
        },
        options: Options(
          headers: {
            "X-API-KEY":"${GetStorage().read("key")}"// Set the content-length.
          },
        ),
      );
      // print(response.data);

    }catch(e){
      print(e);
    }
  }
  FatchGetAuth(context)async{
    String link="${Baseurl}api/get_auth_customer";
    try{
      final response =await Dio().get(link,
        options: Options(
          headers: {
            "X-API-KEY":"${GetStorage().read("key")}"// Set the content-length.
          },
        ),
      );
      print(response.data);
      GetStorage().write("Customer_SlNo", response.data["customer"]["Customer_SlNo"]);
      GetStorage().write("Customer_Name", response.data["customer"]["Customer_Name"]);
      GetStorage().write("Customer_Mobile", response.data["customer"]["Customer_Mobile"]);
      GetStorage().write("Customer_Address", response.data["customer"]["Customer_Address"]);
      GetStorage().write("country_id", response.data["customer"]["country_id"]);
      GetStorage().write("image_name", response.data["customer"]["image_name"]);
      GetStorage().write("Customer_Email", response.data["customer"]["Customer_Email"]);
      GetStorage().write("wallet_balance", response.data["customer"]["wallet_balance"]);
      GetStorage().write("wallet_hang_balance", response.data["customer"]["wallet_hang_balance"]);
      GetStorage().write("status", response.data["customer"]["status"]);
      GetStorage().write("bound", response.data["customer"]["bound"]);
      GetStorage().write("register_bonus", response.data["customer"]["register_bonus"]);
      GetStorage().write("referral_id", response.data["customer"]["referral_id"]);

      print("sAddsafsf ${GetStorage().read("key")}");
      print("sAddsafsf ${GetStorage().read("Customer_SlNo")}");
      print("sAddsafsf ${GetStorage().read("Customer_Name")}");
      print("sAddsafsf ${GetStorage().read("Customer_Mobile")}");
      print("sAddsafsf ${GetStorage().read("Customer_Address")}");
      print("sAddsafsf ${GetStorage().read("country_id")}");
      print("sAddsafsf ${GetStorage().read("image_name")}");
      print("sAddsafsf ${GetStorage().read("Customer_Email")}");
      print("sAddsafsf ${GetStorage().read("wallet_balance")}");
      print("sAddsafsf ${GetStorage().read("referral_id")}");

    }catch(e){
      print(e);
    }
  }


  @override
  void initState() {
    FatchGetAuth(context);
    _listViewController.addListener(_onListScroll);
    box = Hive.box('productBox');
    Future.delayed(Duration(milliseconds: 100),()async {
      _shipperNameController.text=GetStorage().read("Customer_Name");
      _shipperPhoneController.text=GetStorage().read("Customer_Mobile");
      _shipperAddressController.text=GetStorage().read("Customer_Address");
    },);

    Provider.of<All_Product_Provider>(context,listen: false).Api_area_wise_delivery_charge(context);
    super.initState();
  }
  @override
  void dispose() {
    _listViewController.removeListener(_onListScroll);
    super.dispose();
  }
  void _onListScroll() {
    if (_listViewController.position.pixels == _listViewController.position.maxScrollExtent) {
      _scrollViewController.animateTo(
        _scrollViewController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 50),
        curve: Curves.easeIn,
      );
    }

    if (_listViewController.position.pixels == _listViewController.position.minScrollExtent) {
      _scrollViewController.animateTo(
        _scrollViewController.position.minScrollExtent,
        duration: const Duration(milliseconds: 50),
        curve: Curves.easeIn,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    print("key ${GetStorage().read("key")}");
    print("Customer_SlNo ${GetStorage().read("Customer_SlNo")}");
    print("Customer_Name ${GetStorage().read("Customer_Name")}");
    print("Customer_Mobile ${GetStorage().read("Customer_Mobile")}");
    print("Customer_Address ${GetStorage().read("Customer_Address")}");
    print("country_id ${GetStorage().read("country_id")}");
    print("image_name ${GetStorage().read("image_name")}");
    print("Customer_Email ${GetStorage().read("Customer_Email")}");
    print("wallet_balance ${GetStorage().read("wallet_balance")}");
    print("referral_id ${GetStorage().read("referral_id")}");
    // print("chekout from==${widget.checkoutFrom}");
    //
    // print("productName============================${widget.productName}");
    // print("productQuantity==${widget.productQuantity}");
    // print("productUnitPrice==${widget.productUnitPrice}");
    // print("productSubtotalPrice==${widget.productSubtotalPrice}");
    // print("productTotalPrice==${widget.productTotalPrice}");
    // print("Size_Iddd==${widget.Size_id}");
    // print("Color_Iddd==${widget.Collor_id}");
    // print("cashbackammount==${widget.cashbackammount}");
    // print("totalmainprice==${widget.totalmainprice}");
    // print("cashBackPersen==${widget.cashBackPersen}");
    // print("Product_id==${widget.Product_id}");
    // print("=============================");

    double h2TextSize = 15.0;
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    final Api_area_wise_delivery_charge_list=  Provider.of<All_Product_Provider>(context).Api_area_wise_delivery_charse_list;
    vat= int.parse("${GetStorage().read("customer_vat")}") as int ;
    deliveryCharge= double.parse("${GetStorage().read("AAAAAdeliveryCharge")}") as double ;
    Delivery_Charge= int.parse("${GetStorage().read("AAAAAdeliveryCharge")}") ;
    wallet_adjustment= int.parse("${GetStorage().read("wallet_adjustment")}") ;
    wallet_balance = double.parse("${GetStorage().read("wallet_balance")}");


    return  Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back)),
        toolbarHeight: 45,
        backgroundColor: Color.fromARGB(255, 87, 117, 133),
        title: Text('Bigbuy Checkout Page'),
      ),
      body: SingleChildScrollView(
        controller: _scrollViewController,
        child: Container(
          padding: EdgeInsets.all(10),
          // color: Colors.amber,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListView.builder(
                controller: _listViewController,
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                itemCount: box.length,
                itemBuilder: (context, index) {
                  var currentBox = box;
                  var productData = currentBox.getAt(index)!;

                  return Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [

                          Container(
                            width: screenWidth / 3 + 20,
                            child: Text(
                              "${productData.productName}",
                              style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                            ),
                          ),
                          Container(
                            width: screenWidth / 12,
                            child: Text(
                              "${productData.productQuantity}",
                              style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                            ),
                          ),
                          Container(
                            width: screenWidth / 5,
                            child: Text(
                              "৳${productData.productPrice}",
                              textAlign: TextAlign.start,
                              style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                            ),
                          ),
                          Container(
                              width: screenWidth / 4,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    "৳",
                                    style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                  ),
                                  Text(
                                    "${productData.productPrice * productData.productQuantity}",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                                  ),
                                ],
                              ))
                        ],
                      ),
                      Divider()
                    ],
                  );
                },
              ),


              Container(
                margin: EdgeInsets.only(bottom: 15),
                decoration: BoxDecoration(
                  color: const Color.fromARGB(255, 231, 231, 231),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: ListView.builder(
                  itemCount: 1,
                  shrinkWrap: true,
                  physics: ClampingScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    double totalPrice = 0;
                    double totalmainprice = 0;
                    for (int i = 0; i < box.length; i++) {

                      var productData = box.getAt(i)!;
                      totalPrice += productData.productPrice * productData.productQuantity;
                      totalmainprice += productData.main_price * productData.productQuantity;
                      Cashback=totalmainprice-totalPrice;
                      vat_with_total=(vat*totalmainprice)/100;
                      vat_deleverychagre_total = vat_with_total+totalPrice+deliveryCharge;
                      print("productData.main_price  :${productData.main_price}");
                      print("productData.productPrice  :${productData.productPrice}");
                      print("tttttttttttttttttttttttttttttttttttttttttttttttttttt $totalmainprice");
                      print("ttttttttttttttttttttttttttttttgggggggggggggggggggg $totalPrice");
                    }



                    return Container(
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "total: ",
                                style: TextStyle(
                                  fontSize: h2TextSize,
                                ),
                              ),
                              Text(
                                "৳$totalmainprice",
                                style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                              )
                            ],
                          ),
                          Divider(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Cashback: ",
                                style: TextStyle(
                                  fontSize: h2TextSize,
                                ),
                              ),
                              Text(
                                "${Cashback}",
                                style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                              )
                            ],
                          ),
                          Divider(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Subtotal: ",
                                style: TextStyle(
                                  fontSize: h2TextSize,
                                ),
                              ),
                              Text(
                                "${totalPrice}",
                                style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                              )
                            ],
                          ),
                          Divider(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Vat 15% (+): ",
                                style: TextStyle(
                                  fontSize: h2TextSize,
                                ),
                              ),
                              Text(
                                "$vat_with_total",
                                style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black38),
                              )
                            ],
                          ),

                          Divider(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Area:",
                                style: TextStyle(
                                  fontSize: h2TextSize,
                                ),
                              ),
                              /////////////////////////////////////////////////////////////////////////////////////////////////////////////

                              DropdownButton(
                                hint: Text(
                                  'Please choose a location',
                                  style: TextStyle(
                                    fontSize: h2TextSize,
                                  ),
                                ), // Not necessary for Option 1
                                value: _selectedLocation,
                                onChanged: (newValue) {
                                  setState(() {
                                    _selectedLocation = newValue!;

                                    GetStorage().write("AAAAAdistrictName", "${Api_area_wise_delivery_charge_list[int.parse("$newValue")-2].districtName}");
                                    GetStorage().write("AAAAAdistrictSlNo", "${Api_area_wise_delivery_charge_list[int.parse("$newValue")-2].districtSlNo}");
                                    GetStorage().write("AAAAAarea_id", "${Api_area_wise_delivery_charge_list[int.parse("$newValue")-2].areaBranchid}");
                                    GetStorage().write("AAAAAdeliveryOptionId", "${Api_area_wise_delivery_charge_list[int.parse("$newValue")-2].deliveryOptionId}");
                                    GetStorage().write("AAAAAdeliveryCharge", "${Api_area_wise_delivery_charge_list[int.parse("$newValue")-2].deliveryCharge}");
                                    GetStorage().write("AAAAAdeliveryOptionName", "${Api_area_wise_delivery_charge_list[int.parse("$newValue")-2].deliveryOptionName}");
                                    print("AAAAAAAAAAAAAAAAAAA AAAAAdistrictName :${GetStorage().read("AAAAAdistrictName")}");
                                    print("AAAAAAAAAAAAAAAAAAA AAAAAdistrictSlNo : ${GetStorage().read("AAAAAdistrictSlNo")}");
                                    print("AAAAAAAAAAAAAAAAAAA AAAAAarea_id : ${GetStorage().read("AAAAAarea_id")}");
                                    print("AAAAAAAAAAAAAAAAAAA AAAAAdeliveryOptionId : ${GetStorage().read("AAAAAdeliveryOptionId")}");
                                    print("AAAAAAAAAAAAAAAAAAA AAAAAdeliveryCharge : ${GetStorage().read("AAAAAdeliveryCharge")}");
                                    print("AAAAAAAAAAAAAAAAAAA AAAAAdeliveryOptionName : ${GetStorage().read("AAAAAdeliveryOptionName")}");

                                  });
                                },
                                items: Api_area_wise_delivery_charge_list.map((Api_area_wise_delivery_charge_list) {
                                  return DropdownMenuItem(
                                    child: Text(
                                      "${Api_area_wise_delivery_charge_list.districtName}",
                                      style: TextStyle(
                                        fontSize: h2TextSize,
                                      ),
                                    ),
                                    value:  "${Api_area_wise_delivery_charge_list.districtSlNo}",
                                  );
                                }).toList(),
                              ),

                              ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Delivery Charge (+):",
                                style: TextStyle(
                                  fontSize: h2TextSize,
                                ),
                              ),
                              Text(
                                "${GetStorage().read("AAAAAdeliveryCharge") ?? 60}",
                                style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                              ),
                            ],
                          ),
                          Divider(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Total :",
                                style: TextStyle(
                                  fontSize: h2TextSize,
                                ),
                              ),
                              Text(
                                "$vat_deleverychagre_total",
                                style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                              ),
                            ],
                          ),
                          showWalletPay == true
                              ? Column(
                            children: [
                              Divider(),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Wallet Pay :",
                                    style: TextStyle(
                                      fontSize: h2TextSize,
                                    ),
                                  ),
                                  Text(
                                    "$amount",
                                    style: TextStyle(fontSize: h2TextSize, color: Colors.black),
                                  ),
                                ],
                              )
                            ],
                          )
                              : Container(),
                          Divider(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Payable :",
                                style: TextStyle(
                                  fontSize: h2TextSize,
                                ),
                              ),
                              Text(
                                "${vat_deleverychagre_total-amount}",
                                style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black),
                              ),
                            ],
                          ),
                          Divider(),
                          Row(
                            children: [
                              Checkbox(
                                checkColor: Colors.white,
                                activeColor: Colors.black,
                                value: isWalletAdjusted,
                                onChanged: (bool? value) {
                                  setState(() {
                                    amount = (wallet_balance *wallet_adjustment)/100;
                                    print("(wallet_balance *wallet_adjustment)/100  ${amount}");
                                    isWalletAdjusted = value!;
                                    if (isWalletAdjusted) {
                                      print("fdddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
                                      if(wallet_balance>= 0 && amount >= vat_deleverychagre_total){
                                        amount=vat_deleverychagre_total;
                                        showWalletPay = true;
                                        print("fdddddddddddddddddddddddddddddddddddddddddddddddddddddddd $amount");
                                      }
                                      else {
                                        if(wallet_balance>= 0 && amount<vat_deleverychagre_total){
                                          amount;
                                          showWalletPay = true;
                                          print("fdddddddddddddddddddddddddddddddddddddddddddddddddddddddd $amount");
                                        }else{
                                          amount=0;
                                          showWalletPay = true;
                                          print("fdddddddddddddddddddddddddddddddddddddddddddddddddddddddd $amount");
                                        }

                                      }
                                    } else {
                                      showWalletPay = false;
                                    }
                                  });
                                },
                              ),
                              SizedBox(
                                width: 5.0,
                              ),
                              Text(
                                'Wallet Adjustment ($wallet_adjustment%)',
                                style: TextStyle(
                                  fontSize: screenWidth / 24,
                                  color: Colors.lightGreen,
                                ),
                              ),
                            ],
                          ),
                          Divider(),
                          Row(
                            children: [
                              Expanded(
                                flex: 1,
                                child: Row(
                                  children: [
                                    Radio(
                                      value: 0,
                                      groupValue: _selectedRadioButton,
                                      onChanged: (value) {
                                        setState(() {
                                          _selectedRadioButton = value!;
                                        });
                                      },
                                    ),
                                    Text(
                                      'Cash on delivery',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black54,
                                        fontSize: screenWidth / 30,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Row(
                                  children: [
                                    Radio(
                                      value: 1,
                                      groupValue: _selectedRadioButton,
                                      onChanged: (value) {
                                        setState(() {
                                          _selectedRadioButton = value!;
                                        });
                                      },
                                    ),
                                    Text(
                                      'Online Payment',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black54,
                                        fontSize: screenWidth / 30,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    );
                  },
                ),
              ),
              //Shipping Information Section
              Text('Shipping Information', style: TextStyle(fontSize: h2TextSize, fontWeight: FontWeight.bold, color: Colors.black54)),
              Container(
                margin: EdgeInsets.only(top: 15),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.all(8.0),
                        decoration: BoxDecoration(color: Color.fromARGB(255, 231, 231, 231), borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(flex: 1, child: Text("Name *", style: TextStyle(fontSize: h2TextSize))),
                                SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                    flex: 3,
                                    child: MyCustomTextFormField().getCustomEditTextArea(
                                        hintValue: 'Name',
                                        validation: true,
                                        controller: _shipperNameController,
                                        keyboardType: TextInputType.text,
                                        validationErrorMsg: 'error_msg')),
                              ],
                            ),
                            Row(
                              children: [
                                Expanded(flex: 1, child: Text("Phone number *", style: TextStyle(fontSize: h2TextSize))),
                                SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                    flex: 3,
                                    child: MyCustomTextFormField().getCustomEditTextArea(
                                        hintValue: 'Phone number',
                                        validation: true,
                                        controller: _shipperPhoneController,
                                        keyboardType: TextInputType.phone,
                                        validationErrorMsg: 'error_msg')),
                              ],
                            ),
                            Row(
                              children: [
                                Expanded(flex: 1, child: Text("Shipping Address *", style: TextStyle(fontSize: h2TextSize))),
                                SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                    flex: 3,
                                    child: MyCustomTextFormField().getCustomEditTextArea(
                                        hintValue: 'Shipping Address',
                                        validation: true,
                                        controller: _shipperAddressController,
                                        keyboardType: TextInputType.text,
                                        validationErrorMsg: 'error_msg')),
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Expanded(flex: 1, child: Text("Order notes (optional)", style: TextStyle(fontSize: h2TextSize))),
                                SizedBox(
                                  width: 5,
                                ),
                                Expanded(
                                    flex: 3,
                                    child: MyCustomTextFormField().getCustomEditTextArea(
                                      hintValue: 'Notes about your order, e.g. special notes for delivery',
                                      validation: false,
                                      maxLineValue: 2,
                                      controller: _orderNoteController,
                                      keyboardType: TextInputType.text,
                                    )),
                              ],
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            // Align(
                            //     alignment: Alignment.centerRight,
                            //     child: ElevatedButton(
                            //         onPressed: () {
                            //           if (_formKey.currentState!.validate()) {}
                            //         },
                            //         child: Text("Update")))
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),

      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10.0),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(backgroundColor: Color.fromARGB(255, 60, 84, 97), minimumSize: const Size.fromHeight(45)),
          onPressed: () {

           showDialog(context: context, builder: (context) {
             return AlertDialog(
               title: Text("Your order Successfull..!",),

              actions: [
                ActionChip(label: Text("home"),onPressed: () {
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => CustomPageView(
                    selectedIndex: 1,
                  ),));
                },),

                ActionChip(label: Text("Invoice"),onPressed: () {
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MyOrder(),));
                },)
              ],

             );

           },);
            // Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => CustomPageView(
            //   selectedIndex: 1,
            // ),));
            // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            //     duration: Duration(seconds: 1),
            //     content: Text("Order successfull ..? Thanks for Order")));
             Fatch_Orderrrrrr(context)  ;
          },
          child: const Text("Proceed to Pay"),
        ),
      ),
    ) ;
  }
}

 import 'package:badges/badges.dart';
import 'package:bigbuy/Custom/Bootom_NavigationBar/bootom_navigation_bar.dart';
import 'package:bigbuy/Custom/Custom_Card/custom_card.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/custom_drawer.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/end_drawer.dart';
import 'package:bigbuy/FooterSection/about_section.dart';
import 'package:bigbuy/Provider/SubCategory_Provider/all_sub_category_product.dart';
import 'package:bigbuy/custom_page_view.dart';
import 'package:bigbuy/screens/One_Product_Details/one_product_details.dart';

import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:provider/provider.dart';

class SubCategoryProduct extends StatefulWidget {
  SubCategoryProduct({Key? key,required this.category_Idi,this.Categoryname}) : super(key: key);
    int? category_Idi;
    String ? Categoryname;
  @override
  State<SubCategoryProduct> createState() => _SubCategoryProductState();
}
 Box? box = Hive.box('productBox');
class _SubCategoryProductState extends State<SubCategoryProduct> {


 @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  final _key=GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    // print("key ${GetStorage().read("key")}");
    // print("Customer_SlNo ${GetStorage().read("Customer_SlNo")}");
    // print("Customer_Name ${GetStorage().read("Customer_Name")}");
    // print("Customer_Mobile ${GetStorage().read("Customer_Mobile")}");
    // print("Customer_Address ${GetStorage().read("Customer_Address")}");
    // print("country_id ${GetStorage().read("country_id")}");
    // print("image_name ${GetStorage().read("image_name")}");
    // print("Customer_Email ${GetStorage().read("Customer_Email")}");
    // print("wallet_balance ${GetStorage().read("wallet_balance")}");
    // print("referral_id ${GetStorage().read("referral_id")}");


  final All_Sub_Category_ProductList=Provider.of<All_SubCategory_Product_Provider>(context);
  final sub_category_pproduct_list=Provider.of<All_SubCategory_Product_Provider>(context).brandList;
    return Scaffold(

      key: _key,
      endDrawer: End_Add_to_cart_Drawer(),
      drawer: Custom_Drawer_Page(),
      floatingActionButton: FloatingActionButton(onPressed: () {
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => CustomPageView(selectedIndex: 0),));
      },child: Icon(Icons.home,size: 27,)),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: CustomNavigationBarPage(
          Home_color: Colors.white,
          Produc_tColor: Colors.black,
          Category_color: Colors.white,
          setting_Color: Colors.white),
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(onPressed:(){
         Navigator.pop(context);
        }, icon: Icon(Icons.arrow_back,size: 25,color: Colors.black87,),
        ),
        title: Text("${widget.Categoryname}",style: GoogleFonts.poppins(
          fontSize: 18,
          fontStyle: FontStyle.italic,
          letterSpacing: 1,
          fontWeight: FontWeight.bold,
          color: Colors.black,
        ),),

        actions: [
          IconButton(
            iconSize: 28,
            icon: Icon(Icons.search,size: 25,color: Colors.black87,),
            onPressed: () {},
          ),
          Badge(
            position: BadgePosition.custom(
              top: 1,
              end: 5,
            ),
            badgeContent: Text("${box!.length}"),
            child: IconButton(
                onPressed: () {
                  _key.currentState!.openEndDrawer();
                },
                icon: Icon(Icons.shopping_cart,size: 25,color: Colors.black87,)),
          ),
        ],
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: CustomScrollView(
          slivers: [
            SliverPadding(
              padding: EdgeInsets.only(
                top: 10,
                bottom: 0,
                left: 20,
                right: 20,
              ),
              sliver: SliverGrid(
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 300,
                  mainAxisSpacing: 16,
                  crossAxisSpacing: MediaQuery.of(context).size.width*0.04,
                  mainAxisExtent: 285,

                ),
                delegate: SliverChildBuilderDelegate((context, index){
                  return InkWell(
                    onTap: () {
                      Navigator.push(context, MaterialPageRoute(builder: (context) => One_Product_Details(
                        // qty: sub_category_pproduct_list[index].qty,
                        cashbackPercent: sub_category_pproduct_list[index].cashbackPercent,
                        productName: sub_category_pproduct_list[index].productName,
                        mainPrice: sub_category_pproduct_list[index].mainPrice,
                        mainImage:sub_category_pproduct_list[index].mainImage ,
                        productCode: sub_category_pproduct_list[index].productCode,
                        productDescription:sub_category_pproduct_list[index].productDescription ,
                        salePrice: sub_category_pproduct_list[index].salePrice,
                        qty: sub_category_pproduct_list[index].qty,
                        cashbackAmount: sub_category_pproduct_list[index].cashbackAmount,
                        productSlNo: sub_category_pproduct_list[index].productSlNo,

                      ),));
                    },
                    child: Card(
                        elevation: 8.0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(20),
                                topLeft: Radius.circular(20)
                            )
                        ),
                        child: Custom_Card_Page(
                          productSlNo: "${sub_category_pproduct_list[index].productSlNo}",
                          qty: "${sub_category_pproduct_list[index].qty}",
                          mainImage:"https://bigbuy.com.bd/uploads/products/small_image/${sub_category_pproduct_list[index].mainImage}" ,
                          productName:"${sub_category_pproduct_list[index].productName}",
                          mainPrice: "${sub_category_pproduct_list[index].mainPrice}",
                          productDescription:"${sub_category_pproduct_list[index].productDescription}" ,
                          salePrice: "${sub_category_pproduct_list[index].salePrice}",
                          productCode: "${sub_category_pproduct_list[index].productCode}",
                          cashbackPercent: "${sub_category_pproduct_list[index].cashbackPercent}",
                          cashbackAmount: "${sub_category_pproduct_list[index].cashbackAmount}",
                        )),
                  );
                },childCount: sub_category_pproduct_list.length),),
            ),

            SliverToBoxAdapter(
              child: BigBuyFooter(),
            ),
          ],
        ),
      ),
    );
  }
}

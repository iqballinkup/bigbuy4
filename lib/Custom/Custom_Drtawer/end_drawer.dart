import 'package:bigbuy/HiveDemoWithCart/product.dart';
import 'package:bigbuy/screens/CheckOut/riaz_cart_checkout_page.dart';
import 'package:bigbuy/screens/CheckOut/riaz_check_out_page.dart';
import 'package:bigbuy/screens/Login_SignUp/pages/signin_page.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hive_flutter/adapters.dart';

class End_Add_to_cart_Drawer extends StatefulWidget {
  const End_Add_to_cart_Drawer({Key? key}) : super(key: key);

  @override
  State<End_Add_to_cart_Drawer> createState() => _End_Add_to_cart_DrawerState();
}

class _End_Add_to_cart_DrawerState extends State<End_Add_to_cart_Drawer> {
  late final Box box;
  double h1TextSize = 11.0;
  double totalPrice = 0;
  double shippingFee = 60;
  double h2TextSize = 14.0;
  @override
  void initState() {
    box = Hive.box('productBox');
    // Future.delayed(Duration(seconds: 1),(){
    //   Navigator.push(context, MaterialPageRoute(builder: (context) => CartScreen(),));
    // });
    // TODO: implement initState
    super.initState();
  }

  // Delete info from people box
  _deleteProduct(int index) {
    box.deleteAt(index);
    print('Product deleted from box at index: $index');
  }








  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Scaffold(
        appBar: AppBar(
          toolbarHeight: 45,
          backgroundColor: Color.fromARGB(255, 87, 117, 133),
          title: Text('Cart Info'),
          actions: [],
        ),
        body: ValueListenableBuilder(
          valueListenable: box.listenable(),
          builder: (context, Box box, widget) {
            if (box.isEmpty) {
              return Center(
                child: Text('No Product in Cart'),
              );
            } else {
              return Column(
                children: [
                  Expanded(
                    child: ListView.builder(
                      itemCount: box.length,
                      itemBuilder: (context, index) {

                        var currentBox = box;
                        var productData = currentBox.getAt(index)!;

                        return Column(
                          children: [
                            SizedBox(
                              height: 90,
                              child: Card(
                                elevation: 5,
                                color: Colors.white,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                        padding: EdgeInsets.only(),
                                        child: Image.network(
                                          productData.productImage,
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Container(
                                        padding: const EdgeInsets.only(left: 10),
                                        decoration: BoxDecoration(
                                            color: Colors.blueGrey[100],
                                            borderRadius: BorderRadius.only(topRight: Radius.circular(20), bottomRight: Radius.circular(20))),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "${productData.productName}",
                                              style: TextStyle(
                                                color: Colors.black54,
                                                fontWeight: FontWeight.w500,
                                                fontSize: h1TextSize,
                                              ),
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "৳",
                                                  style: TextStyle(
                                                    color: Colors.black54,
                                                    fontSize: h2TextSize,
                                                  ),
                                                ),
                                                Text(
                                                  "${productData.productPrice}",
                                                  style: TextStyle(
                                                    color: Colors.black54,
                                                    fontSize: h2TextSize,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 10,
                                                ),
                                                Text(
                                                  "x ${productData.productQuantity}",
                                                  style: TextStyle(
                                                    color: Colors.black54,
                                                    fontSize: h2TextSize,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  "= ৳",
                                                  style: TextStyle(
                                                    color: Colors.black54,
                                                    fontSize: h2TextSize,
                                                  ),
                                                ),
                                                Text(
                                                  "${productData.productPrice * productData.productQuantity}",
                                                  style: TextStyle(
                                                    color: Colors.black54,
                                                    fontSize: h2TextSize,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                SizedBox(
                                                  height: 25,
                                                  width: 25,
                                                  child: FloatingActionButton(
                                                    heroTag: null,
                                                    onPressed: () {
                                                      setState(() {
                                                        if (productData.productQuantity > 1) {
                                                          productData.productQuantity--;
                                                          ProductDetails existingProduct = box.getAt(index);
                                                          existingProduct.productQuantity = productData.productQuantity--;
                                                          box.putAt(index, existingProduct);
                                                        }
                                                      });
                                                    },
                                                    mini: true,
                                                    child: Icon(
                                                      Icons.remove,
                                                      color: Colors.white,
                                                      size: 25,
                                                    ),
                                                    shape: const CircleBorder(),
                                                    backgroundColor: Colors.black12,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                Text(
                                                  "${productData.productQuantity}",
                                                  style: TextStyle(
                                                    color: Colors.black54,
                                                    fontSize: 20,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                SizedBox(
                                                  height: 25,
                                                  width: 25,
                                                  child: FloatingActionButton(
                                                    heroTag: null,
                                                    onPressed: () {
                                                      setState(() {
                                                        productData.productQuantity++;
                                                        ProductDetails existingProduct = box.getAt(index);
                                                        existingProduct.productQuantity = productData.productQuantity++;
                                                        box.putAt(index, existingProduct);
                                                      });
                                                    },
                                                    mini: true,
                                                    child: Icon(
                                                      Icons.add,
                                                      color: Colors.white,
                                                      size: 25,
                                                    ),
                                                    shape: const CircleBorder(),
                                                    backgroundColor: Colors.black12,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                        flex: 1,
                                        child: Container(
                                          color: Colors.white,
                                          child: FloatingActionButton(
                                            heroTag: null,
                                            onPressed: () {
                                              _deleteProduct(index);
                                              setState(() {
                                                box.length;
                                              });
                                            },
                                            mini: true,
                                            shape: const CircleBorder(),
                                            backgroundColor: Colors.white,
                                            child: Icon(
                                              Icons.delete,
                                              color: Color.fromARGB(255, 231, 113, 139),
                                            ),
                                          ),
                                        ))
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 5,
                            )
                          ],
                        );
                      },
                    ),
                  ),
                  Container(
                    // height: 140,
                    // color: Colors.grey.withOpacity(0.5),
                    child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: 1,
                      itemBuilder: (BuildContext context, int index) {
                        num totalPrice = 0;
                        for (int i = 0; i < box.length; i++) {
                          var productData = box.getAt(i)!;
                          totalPrice += productData.productPrice * productData.productQuantity;
                        }

                        return Container(
                          padding: EdgeInsets.only(top: 10, left: 12, right: 12, bottom: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Subtotal: ",
                                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black38),
                                  ),
                                  Text(
                                    "৳$totalPrice",
                                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black38),
                                  )
                                ],
                              ),
                              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                                Text(
                                  "Shipping cost:",
                                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black38),
                                ),
                                Text(
                                  "৳ $shippingFee",
                                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black38),
                                ),
                              ]),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Total Price:",
                                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black38),
                                  ),
                                  Text(
                                    "৳${(totalPrice + shippingFee).toString()}",
                                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Color.fromARGB(255, 60, 84, 97),
                                  minimumSize: const Size.fromHeight(45),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>GetStorage().read("key")==null ?SignInPage(isRemembered: true,emphone: "01"): CartCheckoutPage(),));
                                },
                                child: const Text("Checkout"),
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  )
                ],
              );
            }
          },
        ),
      ),
    );
  }

  int count = 0;
}

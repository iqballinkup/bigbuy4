

import 'package:bigbuy/Custom/Custom_Drtawer/Drawer_all_Section/my_profile.dart';
import 'package:bigbuy/Custom/Custom_Drtawer/end_drawer.dart';
import 'package:bigbuy/custom_page_view.dart';
import 'package:bigbuy/screens/All_Category/all_category.dart';
import 'package:bigbuy/screens/All_Product/all_product.dart';
import 'package:bigbuy/screens/Cupon/account_user.dart';
import 'package:bigbuy/screens/Login_SignUp/pages/signin_page.dart';
import 'package:bigbuy/screens/home/home.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


class CustomNavigationBarPage extends StatelessWidget {
  CustomNavigationBarPage({Key? key,required this.Home_color, required this.Produc_tColor, required this.Category_color, required this.setting_Color  }) : super(key: key);
  @override
  final Color? Home_color,Produc_tColor,Category_color,setting_Color;

  Widget build(BuildContext context) {
    // TextStyle optionStyle =
    //   GoogleFonts.poppins(fontSize: 18, fontWeight: FontWeight.bold);
    return Container(
         color: Color(0xff076BD6),
      // color: Colors.amber[100],
        height: 50,
        width: double.infinity,
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [

            Expanded(
              child: InkWell(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => CustomPageView(
                    selectedIndex: 1,
                  ),));
                },
                child: Container(
                  width: 80,
                  margin: EdgeInsets.only(
                    left:7,
                  ),
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment:CrossAxisAlignment.center,
                    children: [
                      Icon( Icons.production_quantity_limits,size: Iconsize,color: Produc_tColor,),
                      Text('Product',style:  GoogleFonts.poppins(color:Produc_tColor,fontSize: WordSize, fontWeight: FontWeight.w500),) ,
                    ],
                  ),
                ),
              ),
            ),

            Expanded(
              child: InkWell(
                onTap: () {
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => CustomPageView(
                      selectedIndex: 2,
                  ),));
                },
                child: Container(
                  width: 80,
                  margin: EdgeInsets.only(
                    left:7,
                  ),
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment:CrossAxisAlignment.center,
                    children: [
                      Icon( Icons.category,size: Iconsize,color: Category_color,),
                      Text('Category',style:  GoogleFonts.poppins(color:Category_color,fontSize: WordSize, fontWeight: FontWeight.w500),) ,
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 30,
            ),
            Expanded(
              child: InkWell(
                onTap: () {
                  is_profile==true?  Navigator.push(context, MaterialPageRoute(builder: (context) => CustomPageView(
                    selectedIndex: 3,
                  ),))
                 : Navigator.push(context, MaterialPageRoute(builder: (context) => SignInPage(
                    isRemembered: false,
                    emphone: "01",
                  ),));
                },
                child: Container(
                  width: 90,
                  margin: EdgeInsets.only(
                    left:7,
                  ),
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment:CrossAxisAlignment.center,
                    children: [
                      Icon( Icons.account_circle_outlined,size: Iconsize,color: setting_Color,),
                      Text('profile',style:  GoogleFonts.poppins(color:setting_Color,fontSize: WordSize, fontWeight: FontWeight.w500),) ,
                    ],
                  ),
                ),
              ),
            ),

            Expanded(
              child: InkWell(
                onTap: () {
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => CustomPageView(
                  selectedIndex: 4,
                ),));
                },
                child: Container(
                  width: 90,
                  margin: EdgeInsets.only(
                    left:7,
                  ),
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment:CrossAxisAlignment.center,
                    children: [
                      Icon( Icons.shopping_cart,size: Iconsize,color: Home_color,),
                      Text('cart',style:  GoogleFonts.poppins(color:Home_color,fontSize: WordSize, fontWeight: FontWeight.w500),) ,
                    ],
                  ),
                ),
              ),
            ),

          ],
        )
    );
  }

  double? Iconsize=23;

  double? WordSize=14;

  bool is_profile=true;
}
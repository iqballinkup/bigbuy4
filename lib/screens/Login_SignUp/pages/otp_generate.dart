

import 'package:bigbuy/constants.dart';
import 'package:bigbuy/custom_page_view.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pinput/pinput.dart';

class PinputExample extends StatefulWidget {
    PinputExample({Key? key,
    required this.number,

   }) : super(key: key);

  String number;
  @override
  State<PinputExample> createState() => _PinputExampleState();
}

class _PinputExampleState extends State<PinputExample> {
  final pinController = TextEditingController();
  final focusNode = FocusNode();
  final formKey = GlobalKey<FormState>();
  String ? pinvlue;
  Fatch_Verify_Otp()async{
    String link="${Baseurl}otp-verify";
    print("ppppppppppppppppppppppppppppppppp $pinvlue");
    print("ppppppppppppppppppppppppppppppppp ${GetStorage().read("customer_id")} ");
    try{
      Response response=await Dio().post(link,data: {
         "customer_id":"${GetStorage().read("customer_id")}",
          "otp":"${pinvlue}"
      }
      );
      print(response.data);
      if(response.data=={"already_verified":true}){
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("already_verified")));
      }
    }catch(e){
      print(e);
    }
  }


  @override
  void dispose() {
    pinController.dispose();
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    const focusedBorderColor = Color.fromRGBO(23, 171, 144, 1);
    const fillColor = Color.fromRGBO(243, 246, 249, 0);
    const borderColor = Color.fromRGBO(23, 171, 144, 0.4);

    final defaultPinTheme = PinTheme(
      width: 50,
      height: 60,
      textStyle: const TextStyle(
        fontSize: 22,
        color: Color.fromRGBO(30, 60, 87, 1),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(19),
        border: Border.all(color: borderColor),
      ),
    );


    /// Optionally you can use form to validate the Pinput
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.blue[50],
      body:FractionallySizedBox(widthFactor: 1, child: Form(
        key: formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: double.infinity,
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Verification",style: GoogleFonts.poppins(
                    fontSize: 33,
                    fontWeight: FontWeight.w600,
                    letterSpacing: 1,
                  ),),
                  Container(
                    width: double.infinity,
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(bottom: 20,top: 15),
                    child: Text("Enter the code to sent the number",style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w500,
                      fontSize: 18,
                      color: Colors.black54
                    ),),
                  ),
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.only(bottom: 30),
                    alignment: Alignment.center,
                    child: Text("${widget.number}",style: GoogleFonts.poppins(
                      fontSize: 18,
                      letterSpacing: 1,
                        color: Colors.black54
                    ),),
                  ),
                ],
              ),
            ),

            Directionality(
              // Specify direction if desired
              textDirection: TextDirection.ltr,
              child: Pinput(
                length: 6,
                controller: pinController,
                focusNode: focusNode,
                androidSmsAutofillMethod:
                AndroidSmsAutofillMethod.smsUserConsentApi,
                listenForMultipleSmsOnAndroid: true,
                defaultPinTheme: defaultPinTheme,
                validator: (value) {
                },
                // onClipboardFound: (value) {
                //   debugPrint('onClipboardFound: $value');
                //   pinController.setText(value);
                // },
                hapticFeedbackType: HapticFeedbackType.lightImpact,
                onCompleted: (pin) {
                    pinvlue=pin;
                    print(pinvlue);
                    debugPrint('onCompleted: $pin');
                 //   Fatch_Verify_Otp();
                },
                onChanged: (value) {
                  debugPrint('onChanged: $value');


                },
                cursor: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(bottom: 9),
                      width: 22,
                      height: 1,
                      color: focusedBorderColor,
                    ),
                  ],
                ),
                focusedPinTheme: defaultPinTheme.copyWith(
                  decoration: defaultPinTheme.decoration!.copyWith(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: focusedBorderColor),
                  ),
                ),
                submittedPinTheme: defaultPinTheme.copyWith(
                  decoration: defaultPinTheme.decoration!.copyWith(
                    color: fillColor,
                    borderRadius: BorderRadius.circular(19),
                    border: Border.all(color: focusedBorderColor),
                  ),
                ),
                errorPinTheme: defaultPinTheme.copyBorderWith(
                  border: Border.all(color: Colors.redAccent),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 20,
                bottom: 20
              ),
              padding:EdgeInsets.only(
                right: MediaQuery.of(context).size.width/8,
              ),
              height: 40,
              width: double.infinity,
              alignment: Alignment.centerRight,
              child: InkWell(
                onTap: () {
                  focusNode.unfocus();
                  formKey.currentState!.validate();
                  Fatch_Verify_Otp();
                },
                child: Container(
                    height: 40,
                    width: 70,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(11),
                      color: Colors.blue,
                    ),
                    child: Text("done",style: TextStyle(fontSize: 16,color: Colors.black,fontWeight: FontWeight.w500),)),
              )
            ),
            Container(
              width:double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Didn't receive code..?",style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Colors.black87
                  ),),
                  InkWell(
                    onTap: () async{
                      await Dio().get("https://bigbuy.com.bd/resend-otp/${GetStorage().read("customer_id")}");
                    },
                    child: Text("Resend",style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: Colors.black54
                    ),),
                  ),
                ],
              ),
            )
          ],
        ),
      ),),
    );
  }
  bool is_valid=false;

}
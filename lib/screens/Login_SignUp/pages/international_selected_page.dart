import 'dart:convert';

import 'package:bigbuy/Custom/Custom_Drtawer/Drawer_all_Section/my_profile.dart';
import 'package:bigbuy/constants.dart';
import 'package:bigbuy/screens/Login_SignUp/pages/signin_page.dart';
import 'package:country_list_pick/country_list_pick.dart';
import 'package:country_list_pick/country_selection_theme.dart';
import 'package:country_list_pick/support/code_country.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';


class InternationalSelectedPage extends StatefulWidget {
   InternationalSelectedPage({super.key,this.emailorpn});
  String? emailorpn;
  @override
  State<InternationalSelectedPage> createState() =>
      _InternationalSelectedPageState();
}

class _InternationalSelectedPageState extends State<InternationalSelectedPage> {
 
   bool _obscureText1 = true;
  bool _obscureText2 = true;
  bool isRemembered = false;
  double textFormFieldHeight = 45.0;

  void _toggle1() {
    setState(() {
      _obscureText1 = !_obscureText1;
    });
  }

  void _toggle2() {
    setState(() {
      _obscureText2 = !_obscureText2;
    });
  }

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  final _formkey=GlobalKey<FormState>();
  @override
  void initState() {
    emailController.text="${widget.emailorpn}";
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    var screenWidth = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xffD4EAFF),
        body: Container(
          decoration: BoxDecoration(image: DecorationImage(image: AssetImage(""))),
          padding: const EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Form(
              key: _formkey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Your Name *",
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontSize: screenWidth / 24,
                        color: Colors.black54,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    height: textFormFieldHeight,
                    child: TextFormField(
                      controller: nameController,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Invalid";
                        }
                      },
                      decoration: InputDecoration(
                          errorStyle:  TextStyle(fontSize: 0.01),
                          contentPadding: EdgeInsets.only(
                              left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                          filled: true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder()),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Your email address *",
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontSize: screenWidth / 24,
                        color: Colors.black54,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    height: textFormFieldHeight,
                    child: TextFormField(
                      controller: emailController,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Invalid";
                        }
                      },
                      decoration: InputDecoration(
                          errorStyle:  TextStyle(fontSize: 0.01),
                          contentPadding: EdgeInsets.only(
                              left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                          filled: true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder()),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Country *',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontSize: screenWidth / 24,
                        color: Colors.black54,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CountryListPick(
                    appBar: AppBar(
                      backgroundColor: Colors.amber,
                      title: const Text('Pick your country'),
                    ),
                    // if you need custom picker use this
                    // pickerBuilder: (context, CountryCode countryCode) {
                    //   return Row(
                    //     children: [
                    //       Image.asset(
                    //         countryCode.flagUri,
                    //         package: 'country_list_pick',
                    //       ),
                    //       Text(countryCode.code),
                    //       Text(countryCode.dialCode),
                    //     ],
                    //   );
                    // },
                    theme: CountryTheme(
                      isShowFlag: true,
                      isShowTitle: true,
                      isShowCode: true,
                      isDownIcon: true,
                      showEnglishName: false,
                      labelColor: Colors.blueAccent,
                    ),
                    initialSelection: '+880',
                    onChanged: (CountryCode? code) {
                      print(code!.name);
                      box.write("Country_name", code.name);
                      print(code.code);
                      print(code.dialCode);
                      print(code.flagUri);
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Password *',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontSize: screenWidth / 24,
                        color: Colors.black54,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    height: textFormFieldHeight,
                    child: TextFormField(
                      controller: passwordController,
                      obscureText: _obscureText1,
                      decoration: InputDecoration(
                          errorStyle: const TextStyle(fontSize: 0.01),
                          contentPadding: EdgeInsets.only(
                              left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                          filled: true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder(),
                          suffixIcon: IconButton(
                            icon: Icon(_obscureText1
                                ? Icons.visibility_off
                                : Icons.visibility),
                            onPressed: () {
                              _toggle1();
                            },
                          )),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "invalid";
                        }

                      },
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    'Confirm Password *',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontSize: screenWidth / 24,
                        color: Colors.black54,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    height: textFormFieldHeight,
                    child: TextFormField(
                      controller: confirmPasswordController,
                      obscureText: _obscureText2,
                      decoration: InputDecoration(
                          errorStyle: const TextStyle(fontSize: 0.01),
                          contentPadding: EdgeInsets.only(
                              left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                          filled: true,
                          fillColor: Colors.white,
                          border: OutlineInputBorder(),
                          suffixIcon: IconButton(
                            icon: Icon(_obscureText2
                                ? Icons.visibility_off
                                : Icons.visibility),
                            onPressed: () {
                              _toggle2();
                            },
                          )),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Invalid";
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Checkbox(
                        checkColor: Colors.white,
                        activeColor: Colors.black,
                        value: isRemembered,
                        onChanged: (bool? value) {
                          setState(() {
                            isRemembered = value!;
                          });
                        },
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Row(
                        children: [
                          Text(
                            'I agree to the ',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: screenWidth / 24,
                                color: Colors.black54,
                              ),
                            ),
                          ),
                          Text(
                            'privacy policy',
                            style: GoogleFonts.poppins(
                              decoration: TextDecoration.underline,
                              textStyle: TextStyle(
                                fontSize: screenWidth / 24,
                                color: Colors.black54,
                              ),
                            ),
                          ),
                          Text(
                            ' *',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontSize: screenWidth / 24,
                                color: Colors.black54,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      if (_formkey.currentState!.validate() &&
                          isRemembered == true) {
                        Fatch_registration();
                      }
                      else{
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content:Text("Agree Terms & Condition"),
                          ),
                        );
                      }
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'SIGN UP',
                          style: GoogleFonts.poppins(
                            textStyle: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: screenWidth / 22,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Icon(
                          Icons.forward,
                          color: Colors.white,
                        )
                      ],
                    ),
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.red,
                        minimumSize: const Size.fromHeight(40)),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Already have an account?'),
                      SizedBox(
                        width: 5,
                      ),
                      GestureDetector(
                        onTap: (() {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignInPage(
                                    emphone: "@gmail.com",
                                    isRemembered: true,

                                  )),
                              (route) => false);
                        }),
                        child: Text(
                          'Sign In',
                          style: TextStyle(
                              color: Colors.blue,
                              fontSize: screenWidth / 24,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
   Fatch_registration() async {
     String link = "${Baseurl}add_web_customer";
     try {
       Response response = await Dio().post(link, data: {
         "customer": {
           "bound": "International",
           "Customer_Name": "${nameController.text}",
           "Customer_Mobile": null,
           "Customer_Email": "${emailController.text}",
           "country_id": "${box.read("Country_name")}",
           "password": "${passwordController.text}",
           "confirm_password": "${confirmPasswordController.text}",
           "referral_code": null,
           "agreement": true
         }
       });
       var item=jsonDecode(response.data);
       if(item["create"] ==true){
         Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MyProfile(),));
         box.write("customer_id", item["customer_id"]);
         box.write("customer_Mobile", emailController.text);
         box.write("customer_name", nameController.text);
       }
       if(item["exists"] ==true){
         Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => SignInPage(
           emphone: emailController.text,
           isRemembered: true,
         ),));
         ScaffoldMessenger.of(context).showSnackBar(
           SnackBar(
             content: Text("Already you have an accoun"),
           ),
         );
       }
       print(item);
       print("exists ${item["exists"]}");
       print("create ${item["create"]}");
       print("customer_id ${item["customer_id"]}");
       print("deactive ${item["deactive"]}");
     } catch (e) {
       print(e);
     }
   }
   final box=GetStorage();
}

import 'package:bigbuy/constants.dart';
import 'package:flutter/material.dart';

class CustomSectionPart extends StatelessWidget {
  CustomSectionPart({Key? key,this.FristPart,this.LastPart}) : super(key: key);
  String ? FristPart,LastPart;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          '${FristPart}',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 17,
            color: Colors.black54,
            fontStyle: FontStyle.italic,
          )
          ,
        ),
        Text(
          '${LastPart}',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
            color:Colors.redAccent[100],
            fontStyle: FontStyle.italic,
            decoration:TextDecoration.underline ,
            decorationStyle:TextDecorationStyle.solid,
          )
          ,
        ),
      ],
    );
  }
}



